import { Button, FormControl, Input, InputLabel, makeStyles, Select } from '@material-ui/core';
import { IRegionModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createNewCity } from 'Store/Cities/CitiesReducer';
import { requestAllRegions } from 'Store/Regions/RegionsReducer';
import { TStore } from 'Store/Store';

/** Компонент формы добавления пользователя. */
export const AddCityForm = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();
    /** Список доступных регинов. */
    const regionsList = useSelector<TStore, IRegionModel[]>(store => store.regions.regionsList);

    /** Имя. */
    const [name, setName] = useState<string>('');
    /** Идентификатор региона. */
    const [regionId, setRegionId] = useState<number>(-1);

    useEffect(() => {
        dispatch(requestAllRegions());
    },[]);

    /** Очистить форму. */
    const clearForn = (): void => {
        setName('');
    };

    /** Обработчик добавления города. */
    const handleCreateCity = (): void => {
        const data: any = {
            name: name,
            regionId: regionId,
        };
        dispatch(createNewCity(data));
        clearForn();
    };

    return (
        <div className={classes.container}>
            <Input
                className={classes.input}
                placeholder="Название"
                value={name}
                onChange={(e): void => setName(e.target.value)}
            />
            <FormControl>
                <InputLabel htmlFor="region-select">Регион</InputLabel>
                <Select
                    native
                    value={regionId}
                    onChange={(e): void => setRegionId(e.target.value as number)}
                    inputProps={{
                        name: 'region',
                        id: 'region-select',
                    }}>
                    <option aria-label="None" value={-1}>не выбран</option>
                    {regionsList?.map((region) => (
                        <option key={`region-${region.id}`} value={region.id}>{region.name}</option>
                    ))}
                </Select>
            </FormControl>
            <Button variant="contained" color="secondary" onClick={handleCreateCity}>Добавить</Button>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    input: {
        marginRight: theme.spacing(3),
    }
}));
