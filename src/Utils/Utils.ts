import { DOMAIN, EUserRole } from 'Consts/Consts';

/** Получить коррекрный адрес сервера.
 * 
 * @param path Эндпоинт.
 */
export const getServerUrl = (path: string): string => DOMAIN + path;

/** Название должности из роли. */
export const roleNameConf = {
    [EUserRole.SELLER]: 'Фотограф',
    [EUserRole.CITY_MANAGER]: 'Управляющий',
    [EUserRole.REGION_MANAGER]: 'Регионал',
    [EUserRole.GENERAL_MANAGER]: 'Директор',
    [EUserRole.ADMIN]: 'Админ'
};

/** Имеет ли доступ к сотрудникам.
 * 
 * @param role
 */
export const hasUsersAccess = (role: EUserRole): boolean =>
    role === EUserRole.CITY_MANAGER ||
    role === EUserRole.REGION_MANAGER ||
    role === EUserRole.GENERAL_MANAGER;

/** Имеет ли доступ к точкам.
 * 
 * @param role
 */
export const hasPointsAccess = (role: EUserRole): boolean =>
    role === EUserRole.CITY_MANAGER ||
    role === EUserRole.REGION_MANAGER ||
    role === EUserRole.GENERAL_MANAGER;

/** Имеет ли доступ к городам.
 * 
 * @param role
 */
export const hasCitiesAccess = (role: EUserRole): boolean =>
    role === EUserRole.REGION_MANAGER ||
    role === EUserRole.GENERAL_MANAGER;

/** Имеет ли доступ к регионам.
 * 
 * @param role
 */
export const hasRegionsAccess = (role: EUserRole): boolean =>
    role === EUserRole.GENERAL_MANAGER;
