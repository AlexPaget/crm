import { IUserModel } from 'Models/Models';
import './regionsListItem.scss';

/** Модель свойст компонента. */
interface IProps {
    /** Идентификатор. */
    id: number;
    /** Название. */
    title: string;
    /** Колличество городов. */
    citiesCount?: number;
    /** Управляющий. */
    manager?: IUserModel;
    /** Коллбек клика */
    onClick?: (id: number) => void;
}

/** Компонент элемента списка регионов.
 * 
 * @param props
 */
export const RegionsListItem = (props: IProps): JSX.Element => {
    const { id, title, citiesCount, manager, onClick } = props;

    /** Обработчик клика по элементу. */
    const handleItemLick = (): void => {
        onClick && onClick(id);
    };

    return (
        <div className="regions-list-item" onClick={handleItemLick}>
            <span className="regions-list-item__title">{title}</span>
            {citiesCount && <span>{`к-во городов: ${citiesCount}`}</span>}
            <span>{`Управляющий: ${manager ? manager.userName : 'не назначен'}`}</span>
        </div>
    );
};
