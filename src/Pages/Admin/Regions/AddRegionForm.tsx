import { Button, Input, makeStyles } from '@material-ui/core';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createNewRegion } from 'Store/Regions/RegionsReducer';

/** Компонент формы добавления региона. */
export const AddRegionForm = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Название. */
    const [name, setName] = useState<string>('');

    /** Очистить форму. */
    const clearForm = (): void => {
        setName('');
    };

    /** Обработчик добавления региона. */
    const handleAddCity = (): void => {
        const data: any = {
            name: name
        };
        dispatch(createNewRegion(data));
        clearForm();
    };

    return (
        <div className={classes.container}>
            <Input
                className={classes.input}
                placeholder="Название"
                value={name}
                onChange={(e): void => setName(e.target.value)}
            />
            <Button variant="contained" color="secondary" onClick={handleAddCity}>Добавить</Button>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    input: {
        marginRight: theme.spacing(3),
    }
}));
