import {
    Button,
    Container,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Spinner } from 'Components/LoadingOverlay/Spiner';
import { IRegionModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { requestAllRegions } from 'Store/Regions/RegionsReducer';
import { TStore } from 'Store/Store';
import { AddRegionForm } from './AddRegionForm';

/** Компонент вывода регионов в админ панели. */
export const Regions = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список регинов. */
    const regionsList = useSelector<TStore, IRegionModel[]>(store => store.regions.regionsList);
    /** Индикатор загрузки регионов. */
    const isRegionsLoading = useSelector<TStore, boolean>(store => store.regions.isRegionsLoading);

    /** Флаг отображения формы добавления региона. */
    const [isAddFormVisible, setIsAddFormVisible] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestAllRegions());
    }, []);

    /** Рендер элемента региона.
     * 
     * @param item Элемент списка регионов.
     */
    const renderRegionItem = (item: IRegionModel): JSX.Element => (
        <TableRow key={`region-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.name}</TableCell>
            <TableCell>{item.manager?.userName}</TableCell>
        </TableRow>
    );

    /** Рендер списка регионов. */
    const renderRegionsList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Название</TableCell>
                        <TableCell className={classes.headerCell}>Управляющий</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {regionsList?.map((region) => renderRegionItem(region))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    /** Рендер панели с контролами. */
    const renderControlPanel = (): JSX.Element => (
        <div className={classes.controlPanel}>
            <Button
                onClick={(): void => setIsAddFormVisible((p) => !p)}
                variant="contained"
                color={isAddFormVisible ? 'inherit' : 'primary'}
            >
                {`${isAddFormVisible ? 'скрыть' : 'Добавить регион'}`}
            </Button>
            {isAddFormVisible && <AddRegionForm />}
        </div>
    );

    return (
        <>
            <h1>Регионы</h1>
            <Container>
                {renderControlPanel()}
                {isRegionsLoading ? <Spinner /> : renderRegionsList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
