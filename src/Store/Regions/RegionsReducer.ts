import { API } from 'Api/Api';
import { IRegionModel } from 'Models/Models';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - CitiesReducer. */
export enum ERegionsAction {
    /** Добавить город. */
    ADD_REGION = 'REGIONS/ADD_REGION',
    /** Добавить карточку региона. */
    SET_REGION_CARD = 'regions/SET_REGION_CARD',
    /** Очистить карточку региона. */
    CLEAR_REGION_CARD = 'regions/CREAR_REGION_CARD',
    /** Переключение индикатора загрузки городов. */
    TOGGLE_LOADING_REGIONS = 'REGIONS/TOGGLE_LOADING_REGIONS',
    /** ДОбавить список регионов. */
    ADD_REGIONS_LIST = 'REGIONS/ADD_REGIONS_LIST',
    /** Получить весь список регионов. */
    GET_ALL_REGIONS = 'REGIONS/GET_ALL_REGIONS',
}

/** Модель состояния. */
interface IState {
    /** Список городов. */
    regionsList: IRegionModel[];
    /** Карточка региона. */
    regionCard: IRegionModel | null;
    /** Индикатор загрузки городов. */
    isRegionsLoading: boolean;
}

const initialState: IState = {
    regionsList: [],
    regionCard: null,
    isRegionsLoading: false
};

/** Редюсер пользователей.
 * 
 * @param state
 * @param action 
 */
export const regionsReducer = (state = initialState, action: TReducerAction<typeof regionsAction>): IState => {
    switch (action.type) {
        case (ERegionsAction.ADD_REGION):
            return {
                ...state,
                regionsList: [
                    ...state.regionsList,
                    action.region
                ]
            };
        case (ERegionsAction.SET_REGION_CARD):
            return { ...state, regionCard: action.regionCard };
        case (ERegionsAction.CLEAR_REGION_CARD):
            return { ...state, regionCard: null };
        case (ERegionsAction.TOGGLE_LOADING_REGIONS):
            return { ...state, isRegionsLoading: action.value };
        case (ERegionsAction.ADD_REGIONS_LIST):
            return {
                ...state,
                regionsList: action.regionsList
            };
        default:
            return state;
    }
};

export const regionsAction = {
    /** Добавить регион.
     * 
     * @param region Модель региона.
     */
    addRegion: (region: IRegionModel) => ({ type: ERegionsAction.ADD_REGION, region } as const),
    /** Добавить карточку региона.
     * 
     * @param regionCard
     */
    setRegionCard: (regionCard: IRegionModel) => (
        { type: ERegionsAction.SET_REGION_CARD, regionCard } as const
    ),
    /** Очистить карточку региона. */
    clearRegionCard: () => ({ type: ERegionsAction.CLEAR_REGION_CARD } as const),
    /** Переключить индикатор загрузки регионов.
     * 
     * @param value Значение индикатора.
     */
    toggleRegionsLoading: (value: boolean) => (
        { type: ERegionsAction.TOGGLE_LOADING_REGIONS, value } as const
    ),
    /** Получить список регионов. */
    getAllRegions: () => ({ type: ERegionsAction.GET_ALL_REGIONS } as const),
    /** Добавить список регионов.
     * 
     * @param regionsList Список регионов.
     */
    addRegionsList: (regionsList: IRegionModel[]) => (
        { type: ERegionsAction.ADD_REGIONS_LIST, regionsList } as const
    ),
};

/** Получить список регионов */
export const requestAllRegions = (): ThunkType => (dispatch): any => {
    dispatch(regionsAction.toggleRegionsLoading(true));
    API.GET('api/regions')
        .then((res: any) => {
            dispatch(regionsAction.addRegionsList(res.data));
            dispatch(regionsAction.toggleRegionsLoading(false));
        })
        .catch(() => {
            dispatch(regionsAction.toggleRegionsLoading(false));
        });
};

/** Создать новый регион.
 * 
 * @param data
 */
export const createNewRegion = (data: any): ThunkType => (dispatch): any => {
    dispatch(regionsAction.toggleRegionsLoading(true));
    API.POST('api/regions', data)
        .then((res: any) => {
            dispatch(regionsAction.addRegion(res.data));
            dispatch(regionsAction.toggleRegionsLoading(false));
        })
        .catch(() => dispatch(regionsAction.toggleRegionsLoading(false)));
};

/** Получить регион по ID.
 * 
 * @param id
 */
export const requestRegionById = (id: number): ThunkType => (dispatch): any => {
    dispatch(regionsAction.toggleRegionsLoading(true));
    API.GET(`api/regions/${id}`)
        .then((res: any) => {
            dispatch(regionsAction.setRegionCard(res.data));
            dispatch(regionsAction.toggleRegionsLoading(false));
        })
        .catch(() => dispatch(regionsAction.toggleRegionsLoading(false)));
};

/** Назначить управляющего на регион.
 * 
 * @param params
 */
export const setRegionManager = (params: any): ThunkType => (dispatch): any => {
    dispatch(regionsAction.toggleRegionsLoading(true));
    API.PUT(`api/regions/${params.regionId}`, { managerId: params.managerId })
        .then(() => {
            dispatch(regionsAction.toggleRegionsLoading(false));
        })
        .catch(() => dispatch(regionsAction.toggleRegionsLoading(false)));
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof regionsAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
