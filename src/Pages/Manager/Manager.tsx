import { IAuthData } from 'Models/Models';
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, Redirect, Route, Switch, useHistory, useRouteMatch } from 'react-router-dom';
import { AuthService } from 'Services/AuthService';
import { TStore } from 'Store/Store';
import { requestUserInfo } from 'Store/Users/UsersReducer';
import { hasCitiesAccess, hasPointsAccess, hasRegionsAccess, hasUsersAccess, roleNameConf } from 'Utils/Utils';
import { CitiesContent } from './Content/CitiesContent/CitiesContent';
import { MainContent } from './Content/MainContent.tsx/MainContent';
import { PointsContent } from './Content/PointsContent/PointsContent';
import { RegionsContent } from './Content/RegionsContent.tsx/RegionsContent';
import { ReportsContent } from './Content/ReportsContent/ReportsContent';
import { WorkersContent } from './Content/WorkersContent/WorkersContent';
import './manager.scss';

/** Элемент меню */
interface ISideBarMenuItem {
    /** Идентификатор */
    id: string;
    /** Заголовок */
    title: string;
    /** Иконка. */
    icon?: string;
    /** Страница */
    url: string;
    /** Флаг активной страницы */
    isActive?: boolean;
    /** Обработчик клика. */
    onClick?: () => void;
}

/** Компонент Главной страницы. */
export const Manager = (): JSX.Element => {
    const { path } = useRouteMatch();

    const history = useHistory();
    const dispatch = useDispatch();

    const user = useSelector<TStore, IAuthData | null>(store => store.auth.data);

    useEffect(() => {
        dispatch(requestUserInfo());
    }, []);

    /** Получить элементы меню сайдбара. */
    const getMenuItems = (): ISideBarMenuItem[] => {
        const role = user?.role;
        const result = [{
            id: 'side-bar-main',
            title: 'Главная',
            url: '/',
        }];
        if (role) {
            hasUsersAccess(role) && result.push({
                id: 'side-bar-workers',
                title: 'Сотрудники',
                url: '/users',
            });
            hasPointsAccess(role) && result.push({
                id: 'side-bar-points',
                title: 'Точки',
                url: '/points',
            });
            hasCitiesAccess(role) && result.push({
                id: 'side-bar-cities',
                title: 'Города',
                url: '/cities',
            });
            hasRegionsAccess(role) && result.push({
                id: 'side-bar-regions',
                title: 'Регионы',
                url: '/regions',
            });
        }
        result.push({
            id: 'side-bar-reports',
            title: 'Отчеты',
            url: '/reports',
        });

        return result;
    };

    /** Обработчик кнопки выхода из аккаунта. */
    const handleLogoutClick = useCallback((): void => {
        AuthService.logout(() => history.push('/auth'));
    }, []);

    /** Рендер элемента списка меню.
     * 
     * @param item
     */
    const renderSideBarItem = (item: ISideBarMenuItem): JSX.Element => (
        <li
            key={item.id}
            className="manager__sidebar-list-item"
        >
            <NavLink
                to={item.url}
                className="manager__sidebar-navlink"
            >
                {item.title}
            </NavLink>
        </li>
    );

    /** Рендер сайдбара */
    const renderSideBar = (): JSX.Element => (
        <div className="manager__sidebar">
            <div className="manager__sidebar-logo-container">
                <img className="manager__sidebar-logo" src={`${process.env.PUBLIC_URL}/logo.png`} />
            </div>
            <ul className="manager__sidebar-list">
                {getMenuItems().map((item) => renderSideBarItem(item))}
            </ul>
        </div>
    );

    return (
        <div className="manager__wrapper">
            {renderSideBar()}
            <div className="manager__content-container">
                <div className="manager__topbar">
                    <span>{user?.role && roleNameConf[user.role]}</span>
                    <span className="manager__exit-btn" onClick={handleLogoutClick}>Выход</span>
                </div>
                <div className="manager__content">
                    <Switch>
                        <Route exact path={path}>
                            <MainContent />
                        </Route>
                        <Route path={'/users'}>
                            <WorkersContent />
                        </Route>
                        <Route path={'/points'}>
                            <PointsContent />
                        </Route>
                        <Route path={'/cities'}>
                            <CitiesContent />
                        </Route>
                        <Route path={'/regions'}>
                            <RegionsContent />
                        </Route>
                        <Route path={'/reports'}>
                            <ReportsContent />
                        </Route>
                        <Redirect to={path} />
                    </Switch>
                </div>
            </div>
        </div>
    );
};
