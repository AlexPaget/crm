import { IMarketModel } from 'Models/Models';
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { Route, Switch, useRouteMatch, useHistory } from 'react-router-dom';
import { requestAllMarkets } from 'Store/Markets/MarketsReducer';
import { PointsListItem } from './PointsListItem/PointsListItem';
import { PointCard } from './PointCard/PointCard';

/** Компонент страницы точки. */
export const PointsContent: FC = (): JSX.Element => {
    const { path } = useRouteMatch();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Список точек. */
    const marketsList = useSelector<TStore, IMarketModel[]>(store => store.markets.marketsList);

    useEffect(() => {
        dispatch(requestAllMarkets());
    }, []);

    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/points/${id}`);
    };

    /** Рендер списка точек. */
    const renderMarketsList = (): JSX.Element => (
        <>
            <h2>Рабочие точки</h2>
            <div>
                {marketsList && marketsList.map((item) => (
                    <PointsListItem
                        key={`city-${item.id}`}
                        id={item.id}
                        title={item.name}
                        adress={item.adress}
                        sellersCount={item.sellers?.length ?? 0}
                        city={item.city}
                        onClick={handleItemClick}
                    />
                ))}
            </div>
        </>
    );

    return (
        <div>
            <Switch>
                <Route exact path={path}>
                    {renderMarketsList()}
                </Route>
                <Route path={`${path}/:id`}>
                    <PointCard />
                </Route>
            </Switch>
        </div>
    );
};
