import { EUserRole } from 'Consts/Consts';
import { IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { TStore } from 'Store/Store';
import { requestUserById, usersAction } from 'Store/Users/UsersReducer';
import { roleNameConf } from 'Utils/Utils';

/** Карточка пользователя.
 *
 * @param props
 */
export const UserCard = (): JSX.Element => {
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Список пользователей. */
    const user = useSelector<TStore, IUserModel | null>(store => store.users.userCard);

    useEffect(() => {
        dispatch(requestUserById(id));

        return (): void => {
            dispatch(usersAction.clearUserCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };

    /** Рендер места занимаемой должности. */
    const renderRoleLocation = (): JSX.Element => {
        const role = user?.account?.role;
        if (!role) {
            return <p>Не назначен</p>;
        }
        if (role === EUserRole.SELLER) {
            return user?.market?.name
                ? <p>{`Продавец в ${user.market.name} (${user.market.city})`}</p>
                : <p>торговая точка не назначена</p>;
        }
        if (role === EUserRole.CITY_MANAGER) {
            return user?.cityManagerIn?.name
                ? <p>{`Управляющий по городу ${user.cityManagerIn.name}`}</p>
                : <p>город управления не назначен</p>;
        }
        if (role === EUserRole.REGION_MANAGER) {
            return user?.regionManagerIn?.name
                ? <p>{`Управляющий по региону ${user.regionManagerIn.name}`}</p>
                : <p>регион управления не назначен</p>;
        }

        return <p>something wrong...</p>;
    };

    return (
        <div>
            <button onClick={handleBackButtonPress}>назад</button>
            <h3>{user?.userName}</h3>
            <p>город: {user?.city}</p>
            <hr />
            {user?.account?.role && (
                <p>{roleNameConf[user.account.role]}</p>
            )}
            <br />
            <div>
                {renderRoleLocation()}
            </div>
        </div>
    );
};
