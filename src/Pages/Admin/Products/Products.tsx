import {
    Button,
    Container,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Spinner } from 'Components/LoadingOverlay/Spiner';
import { IProductModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { requestAllProducts } from 'Store/Products/ProductsReducer';
import { TStore } from 'Store/Store';
import { AddProductForm } from './AddProductForm';

/** Компонент вывода товаров в админ панели. */
export const Products = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список товаров. */
    const productsList = useSelector<TStore, IProductModel[]>(store => store.products.productsList);
    /** Индикатор загрузки товаров. */
    const isLoading = useSelector<TStore, boolean>(store => store.products.isLoading);

    /** Флаг отображения формы добавления нового товара. */
    const [isAddFormVisible, setIsAddFormVisible] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestAllProducts());
    }, []);

    /** Рендер элемента товара.
     * 
     * @param item Элемент списка товаров.
     */
    const renderProductItem = (item: IProductModel): JSX.Element => (
        <TableRow key={`product-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.name}</TableCell>
        </TableRow>
    );

    /** Рендер списка товаров. */
    const renderProductsList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Название</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {productsList?.map((product) => renderProductItem(product))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    /** Рендер панели с контролами. */
    const renderControlPanel = (): JSX.Element => (
        <div className={classes.controlPanel}>
            <Button
                onClick={(): void => setIsAddFormVisible((p) => !p)}
                variant="contained"
                color={isAddFormVisible ? 'inherit' : 'primary'}
            >
                {`${isAddFormVisible ? 'скрыть' : 'Добавить товар'}`}
            </Button>
            {isAddFormVisible && <AddProductForm />}
        </div>
    );

    return (
        <>
            <h1>Товары</h1>
            <Container>
                {renderControlPanel()}
                {isLoading ? <Spinner /> : renderProductsList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
