import {
    Container,
    Table,
    TableContainer,
    TableHead,
    TableRow,
    TableBody,
    TableCell,
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { requestAllUsers } from 'Store/Users/UsersReducer';
import { roleNameConf } from 'Utils/Utils';

/** Компонент вывода Пользователей. */
export const Users = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список пользователей. */
    const usersList = useSelector<TStore, IUserModel[]>(store => store.users.usersList);

    useEffect(() => {
        dispatch(requestAllUsers());
    }, []);

    /** Рендер элемента пользователя.
     * 
     * @param item Элемент списка пользователей.
     */
    const renderUserItem = (item: IUserModel): JSX.Element => (
        <TableRow key={`user-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.id}</TableCell>
            <TableCell component="th" scope="row">{item.userName}</TableCell>
            <TableCell component="th" scope="row">
                {item.account?.role ? roleNameConf[item.account.role] : ''}
            </TableCell>
            <TableCell component="th" scope="row">{item.city ?? ''}</TableCell>
        </TableRow>
    );

    /** Рендер списка сотрудников. */
    const renderUsersList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>ID</TableCell>
                        <TableCell className={classes.headerCell}>Имя</TableCell>
                        <TableCell className={classes.headerCell}>Должность</TableCell>
                        <TableCell className={classes.headerCell}>Город</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {usersList && usersList?.map((user) => renderUserItem(user))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    return (
        <>
            <h1>Сотрудники</h1>
            <Container>
                {renderUsersList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
