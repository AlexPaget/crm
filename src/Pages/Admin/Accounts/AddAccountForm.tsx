import { Button, Input, makeStyles, Select } from '@material-ui/core';
import { EUserRole } from 'Consts/Consts';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createNewAccount } from 'Store/Accounts/AccountsReducer';
import { roleNameConf } from 'Utils/Utils';

/** Компонент формы добавления аккаунта. */
export const AddAccountForm = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Логин. */
    const [login, setLogin] = useState<string>('');
    /** Роль. */
    const [role, setRole] = useState<EUserRole>(EUserRole.SELLER);

    /** Очистить форму. */
    const clearForn = (): void => {
        setLogin('');
        setRole(EUserRole.SELLER);
    };

    /** Обработчик добавления города. */
    const handleAddCity = (): void => {
        const data: any = {
            login: login,
            role: role,
        };
        dispatch(createNewAccount(data));
        clearForn();
    };

    return (
        <div className={classes.container}>
            <Input
                className={classes.input}
                placeholder="Логин"
                value={login}
                onChange={(e): void => setLogin(e.target.value)}
            />
            <Select
                native
                value={role}
                onChange={(e): void => setRole(e.target.value as EUserRole)}
                inputProps={{
                    name: 'role',
                    id: 'role-select',
                }}>
                <option value={EUserRole.SELLER}>{roleNameConf[EUserRole.SELLER]}</option>
                <option value={EUserRole.CITY_MANAGER}>{roleNameConf[EUserRole.CITY_MANAGER]}</option>
                <option value={EUserRole.REGION_MANAGER}>{roleNameConf[EUserRole.REGION_MANAGER]}</option>
                <option value={EUserRole.GENERAL_MANAGER}>{roleNameConf[EUserRole.GENERAL_MANAGER]}</option>
                <option value={EUserRole.ADMIN}>{roleNameConf[EUserRole.ADMIN]}</option>
            </Select>
            <Button variant="contained" color="secondary" onClick={handleAddCity}>Добавить</Button>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    input: {
        marginRight: theme.spacing(3),
    }
}));
