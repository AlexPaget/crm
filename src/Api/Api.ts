import axios from 'axios';
import { getServerUrl } from 'Utils/Utils';

/** Класс API обёртка над axios. */
export class API {
    /** Медот запроса GET.
     * 
     * @param path адрес запроса.
     */
    public static GET(path: string): any {
        return axios.get(
            getServerUrl(path),
            {
                withCredentials: true,
            }
        );
    }

    /** Метод запроса POST
     * 
     * @param path адрес запроса.
     * @param body тело запроса.
     */
    public static POST(path: string, body: any): any {

        return axios.post(
            getServerUrl(path),
            body,
            { withCredentials: true }
        );
    }

    /** Метод запроса PUT
     * 
     * @param path адрес запроса.
     * @param body тело запроса.
     */
    public static PUT(path: string, body: any): any {

        return axios.put(
            getServerUrl(path),
            body,
            { withCredentials: true }
        );
    }

    /** Метод запроса DELETE
     * 
     * @param path адрес запроса.
     */
    public static DELETE(path: string): any {

        return axios.delete(
            getServerUrl(path),
            { withCredentials: true }
        );
    }
};
