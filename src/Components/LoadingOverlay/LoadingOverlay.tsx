import { Backdrop, CircularProgress } from '@material-ui/core';
import './loadingOverlay.css';

/** Компонент индикатора загрузки. */
export const LoadingOverlay = (): JSX.Element => (
    <Backdrop open className="loading-overlay" >
        <CircularProgress color='primary' />
    </Backdrop>
);
