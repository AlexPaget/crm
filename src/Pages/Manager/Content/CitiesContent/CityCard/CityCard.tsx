import { FormControl, InputLabel, Select } from '@material-ui/core';
import { EUserRole } from 'Consts/Consts';
import { ICityModel, IUserModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory, NavLink } from 'react-router-dom';
import { citiesAction, requestCityById, setCityManager } from 'Store/Cities/CitiesReducer';
import { TStore } from 'Store/Store';
import { requestUsersByRole } from 'Store/Users/UsersReducer';

/** Карточка города.
 *
 * @param props
 */
export const CityCard = (): JSX.Element => {
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Модель города. */
    const cityCard = useSelector<TStore, ICityModel | null>(store => store.cities.cityCard);
    /** Список доступных менеджеров. */
    const managersList = useSelector<TStore, IUserModel[]>(store => store.users.usersList);

    /** Режим изменения управляющего. */
    const [isEditManager, setIsEditManager] = useState<boolean>(false);
    /** Выбранный менеджер */
    const [
        selectedManagerId,
        setSelectedManagerId
    ] = useState<number>(cityCard?.manager?.id ?? -1);

    useEffect(() => {
        dispatch(requestCityById(id));
        dispatch(requestUsersByRole(EUserRole.CITY_MANAGER));

        return (): void => {
            dispatch(citiesAction.clearCityCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };

    /** Обработчик кнопки выбора менеджера. */
    const handleSelectManager = (): void => {
        const managerId = selectedManagerId === -1 ? null : selectedManagerId;
        const params: any = {
            cityId: id,
            managerId: managerId,
        };
        dispatch(setCityManager(params));
        setIsEditManager(false);
    };

    /** Рендер списка городов. */
    const renderCities = (): JSX.Element => (
        <div>
            <p>точки в городе:</p>
            {cityCard?.markets?.map((item) => (
                <NavLink
                    key={`market-${item.id}`}
                    className="navlink--text"
                    to={`/points/${item.id}`}
                >
                    {item.name}
                </NavLink>
            ))
            }
        </div>
    );

    /** Рендер менеджера. */
    const renderManagerData = (): JSX.Element => (
        <div>
            <span>Управляющий: </span>
            {cityCard?.manager?.userName
                ? <NavLink
                    className="navlink--text"
                    to={`/users/${cityCard.manager.id}`}
                >
                    {cityCard.manager.userName}
                </NavLink>
                : <span>не назначен</span>}
            <button onClick={(): void => setIsEditManager(true)}>Изменить</button>
        </div>
    );

    /** Рендер селекта выбора управляющего. */
    const renderManagerSelect = (): JSX.Element => (
        <FormControl>
            <InputLabel htmlFor="region-select">Выбрать менеджера</InputLabel>
            <Select
                native
                value={selectedManagerId}
                onChange={(e): void => setSelectedManagerId(e.target.value as number)}
                inputProps={{
                    name: 'manager',
                    id: 'manager-select',
                }}>
                <option aria-label="None" value={-1}>не выбран</option>
                {managersList?.map((m) => (
                    <option key={`manager-${m.id}`} value={m.id}>{m.userName}</option>
                ))}
            </Select>
            <button onClick={handleSelectManager}>Выбрать</button>
        </FormControl>
    );

    return (
        <div>
            <button onClick={handleBackButtonPress}>назад</button>
            {cityCard && <h2>{cityCard?.name}</h2>}
            {renderCities()}
            {isEditManager ? renderManagerSelect() : renderManagerData()}
        </div>
    );
};
