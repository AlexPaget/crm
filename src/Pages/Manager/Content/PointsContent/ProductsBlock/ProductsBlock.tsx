import {
    Button,
    Dialog,
    DialogTitle,
    ListItem,
    Popover,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField,
    Typography
} from '@material-ui/core';
import { IProductModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestAvailableProducts } from 'Store/Products/ProductsReducer';
import { TStore } from 'Store/Store';
import AddIcon from '@material-ui/icons/Add';
import './productsBlock.scss';
import { useParams } from 'react-router-dom';
import { addProductToMarket, removeProductFromMarket, updateProductFromMarket } from 'Store/Markets/MarketsReducer';
import { ProductItem } from './ProductItem';

/** Свойства компонента. */
interface IProps {
    /** Товары. */
    products?: IProductModel[];
}

/** Компонент блока товаров на рабочей точке.
 * 
 * @param props Свойства компонента
 */
export const ProductsBlock = (props: IProps): JSX.Element => {
    const { products } = props;

    const { id } = useParams<any>();
    const dispatch = useDispatch();

    /** Список товаров. */
    const availableProducts =
        useSelector<TStore, IProductModel[]>(store => store.products.availableProducts);

    const [anchorEl, setAnchorEl] = useState(null);
    const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
    const [editProductItem, setEditProductItem] = useState<IProductModel | null>(null);
    /** Значение заголовка в модальном окне. */
    const [editProductTitle, setEditProductTitle] = useState<string>('');
    /** Значение в поле ввода изменения цены. */
    const [priceValue, setPriceValue] = useState<number>(0);
    /** Значение в поле ввода изменения колличества. */
    const [countValue, setCountValue] = useState<number>(0);

    useEffect(() => {
        dispatch(requestAvailableProducts(id));
    }, []);

    useEffect(() => {
        if (editProductItem === null) {
            setPriceValue(0);
            setEditProductTitle('');
            setCountValue(0);
        }
        if (editProductItem) {
            if (editProductItem.ProductMarket) {
                setPriceValue(editProductItem?.ProductMarket.price);
                setCountValue(editProductItem?.ProductMarket.count);
            }
            setEditProductTitle(editProductItem.name);
        }
    }, [editProductItem]);

    /** Обработчик открытия поповера.
     * 
     * @param event
     */
    const handlePopoverClick = (event: any): void => {
        event?.currentTarget && setAnchorEl(event.currentTarget);
    };

    /** Обработчик закрытия поповера. */
    const handlePopoverClose = (): void => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const popoverId = open ? 'simple-popover' : undefined;

    /** Обработчик удаления товара.
     * 
     * @param productId
     */
    const handleRemoveProduct = (productId: number): void => {
        dispatch(removeProductFromMarket({
            marketId: id,
            productId
        }));
    };

    /** Обработчик сохранения изменённой информации. */
    const handleEditProduct = (): void => {
        setIsDialogOpen(false);
        editProductItem?.id && dispatch(updateProductFromMarket({
            marketId: id,
            productId: editProductItem.id,
            price: priceValue,
            count: countValue,
        }));
    };

    /** Обработчик изменения товара.
     * 
     * @param productItem
     */
    const handleClickEdit = (productItem: IProductModel): void => {
        setEditProductItem(productItem);
        setIsDialogOpen(true);
    };

    /** Обработчик добавления товара.
     * 
     * @param productId
     */
    const handleAddProduct = (productId: number): void => {
        dispatch(addProductToMarket({
            marketId: id,
            productId
        }));
    };

    /** Рендер доступных продуктов. */
    const renderAvailableProducts = (): JSX.Element => (
        <div className="products-block__available-products-list">
            {availableProducts?.map((item) => (
                <div
                    key={item.id}
                    className="products-block__available-item"
                    onClick={(): void => handleAddProduct(item.id)}
                >
                    <AddIcon className="products-block__add-icon" />
                    <span>{item.name}</span>
                </div>
            ))}
        </div>
    );

    /** Рендер доступных товаров. */
    const renderProducts = (): JSX.Element | JSX.Element[] => (
        <Table aria-label="customized table">
            <TableHead className="products-block__table-head">
                <TableRow>
                    <TableCell size="small">Название</TableCell>
                    <TableCell size="small" align="center">Количество</TableCell>
                    <TableCell size="small" align="center">Цена</TableCell>
                    <TableCell />
                </TableRow>
            </TableHead>
            <TableBody>
                {products?.map((p) => (
                    <ProductItem
                        id={p.id}
                        key={`product-item-${p.id}`}
                        name={p.name}
                        count={p.ProductMarket.count}
                        price={p.ProductMarket.price}
                        onEdit={(): void => handleClickEdit(p)}
                        onRemove={handleRemoveProduct}
                    />
                ))}
            </TableBody>
        </Table>
    );

    /** Обработчик закрытия окна. */
    const handleCloseDialog = (): void => {
        setIsDialogOpen(false);
        setEditProductItem(null);
    };

    /** Рендер диалогового окна. */
    const renderDialog = (): JSX.Element => (
        <Dialog onClose={handleCloseDialog} open={isDialogOpen}>
            <DialogTitle>{editProductTitle}</DialogTitle>
            <ListItem>
                <TextField
                    variant="outlined"
                    label="Цена"
                    type="number"
                    value={priceValue}
                    onChange={({ target }): void => setPriceValue(Number(target.value))}
                />
            </ListItem>
            <ListItem>
                <TextField
                    variant="outlined"
                    label="Количество"
                    type="number"
                    value={countValue}
                    onChange={({ target }): void => setCountValue(Number(target.value))}
                />
            </ListItem>
            <ListItem>
                <Button
                    className="products-block__button-confirm"
                    variant="contained"
                    color="primary"
                    onClick={handleEditProduct}
                >Сохранить</Button>
            </ListItem>
        </Dialog>
    );

    return (
        <div className="products-block">
            <div className="products-block__header">
                <p className="products-block__title">Товары</p>
                <div>
                    <Button
                        aria-describedby={id}
                        variant="contained"
                        onClick={handlePopoverClick}
                    >
                        Добавить
                    </Button>
                    <Popover
                        id={popoverId}
                        open={open}
                        anchorEl={anchorEl}
                        onClose={handlePopoverClose}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                    >
                        {availableProducts
                            ? renderAvailableProducts()
                            : <Typography>Нет доступных продуктов</Typography>}
                    </Popover>
                    {renderDialog()}
                </div>
            </div>
            <div className="products-block__list">
                {products?.length
                    ? renderProducts()
                    : <Typography
                        align="center"
                        className="products-block__empty"
                    >нет добавленных товаров</Typography>
                }
            </div>
        </div>
    );
};
