import { FC } from 'react';

/** Компонент главной страницы. */
export const MainContent: FC = (): JSX.Element => (
    <h1>Главная страница</h1>
);
