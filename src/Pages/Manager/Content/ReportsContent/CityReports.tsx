import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestCityReports } from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода городских отчетов. */
export const CityReports = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();
    /** Отчеты. */
    const cityReports = useSelector<TStore, any[]>(store => store.report.cityReports);
    /** Текущий потльзователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);

    useEffect(() => {
        if (currentUser?.managedCityId){
            dispatch(requestCityReports(currentUser.managedCityId));
        }
    }, [currentUser]);

    return (
        <div className="start-shift__reports-container">
            <h3>Последние отчеты</h3>
            <TableContainer component={Paper}>
                <Table size="small" aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Дата</TableCell>
                            <TableCell className={classes.headerCell}>Выручка</TableCell>
                            <TableCell className={classes.headerCell}>Расходы</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {cityReports && cityReports?.map((r) => (
                            <TableRow key={`city-report-item-${r.id}`}>
                                <TableCell align="center" component="th" scope="row">{r.date}</TableCell>
                                <TableCell align="center" component="th" scope="row">{r.total}</TableCell>
                                <TableCell align="center" component="th" scope="row">{r.expenses}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
