import { API } from 'Api/Api';
import { IMarketModel } from 'Models/Models';
import { BaseThunkType, store, TReducerAction } from 'Store/Store';

/** Actions - shiftReducer. */
export enum EShiftAction {
    /** Установить Id активной смены. */
    SET_ACTIVE_SHIFT = 'shift/SET_ACTIVE_SHIFT',
    /** Добавить доступные точки для открытия смены. */
    SET_AVAILABLE_MARKETS = 'shift/SET_SHIFT_AVAILABLE_MARKETS',
    /** Переключить индиактор загрузки. */
    TOGGLE_LOADING = 'shift/TOGGLE_LOADING',
    /** Добавить рабочую точку. */
    SET_CURRENT_MARKET_SHIFT = 'shift/SET_CURRENT_MARKET_SHIFT',
    /** Добавить список заказов. */
    SET_ORDERS_LIST = 'shift/SET_ORDERS_LIST',
    /** Добавить список отчетов. */
    SET_REPORTS_LIST = 'shift/SET_REPORTS_LIST'
}

/** Модель состояния. */
interface IState {
    /** Идентификатор активной смены. */
    activeShift: any | null;
    /** Список доступных для смены точек. */
    availableMarkets: IMarketModel[];
    /** Индикатор загрузки. */
    isLoading: boolean;
    /** Список операций. */
    ordersList: any[];
    /** Текущая рабочая точка. */
    currentMarketShift: IMarketModel | null;
    /** Список отчетов. */
    reportsList: any[];
}

const initialState: IState = {
    activeShift: null,
    availableMarkets: [],
    isLoading: false,
    ordersList: [],
    currentMarketShift: null,
    reportsList: []
};

/** Редюсер рабочей смены.
 * 
 * @param state
 * @param action 
 */
export const shiftReducer = (state = initialState, action: TReducerAction<typeof shiftAction>): IState => {
    switch (action.type) {
        case (EShiftAction.TOGGLE_LOADING):
            return {
                ...state,
                isLoading: action.value
            };
        case (EShiftAction.SET_ACTIVE_SHIFT):
            return {
                ...state,
                activeShift: action.activeShift
            };
        case (EShiftAction.SET_AVAILABLE_MARKETS):
            return {
                ...state,
                availableMarkets: action.availableMarkets
            };
        case (EShiftAction.SET_ORDERS_LIST):
            return {
                ...state,
                ordersList: action.ordersList
            };
        case (EShiftAction.SET_CURRENT_MARKET_SHIFT):
            return {
                ...state,
                currentMarketShift: action.currentMarketShift
            };
        case (EShiftAction.SET_REPORTS_LIST):
            return {
                ...state,
                reportsList: action.reportsList
            };
        default:
            return state;
    }
};

export const shiftAction = {
    /** Добавить доступные точки для открытия смены.
     * 
     * @param availableMarkets
     */
    setAvailableMarkets: (availableMarkets: IMarketModel[]) => (
        { type: EShiftAction.SET_AVAILABLE_MARKETS, availableMarkets } as const
    ),
    /** Добавить текущую рабочую точку.
     * 
     * @param currentMarketShift
     */
    setCurrentMarketShift: (currentMarketShift: IMarketModel) => (
        { type: EShiftAction.SET_CURRENT_MARKET_SHIFT, currentMarketShift } as const
    ),
    /** Добавить идентификатор активной смены.
     * 
     * @param activeShift
     */
    setActiveShift: (activeShift: any) => (
        { type: EShiftAction.SET_ACTIVE_SHIFT, activeShift } as const
    ),
    /** Добавить список операций.
     * 
     * @param ordersList
     */
    setOrdersHistory: (ordersList: any[]) => (
        { type: EShiftAction.SET_ORDERS_LIST, ordersList } as const
    ),
    /** Добавить список отчетов.
     * 
     * @param reportsList
     */
    setReportsList: (reportsList: any[]) => (
        { type: EShiftAction.SET_REPORTS_LIST, reportsList } as const
    ),
    /** Переключить индикатор загрузки регионов.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EShiftAction.TOGGLE_LOADING, value } as const
    ),
};

/** Получить список доступных точек для открытия смены.
 * 
 * @param id
 */
export const requestAvailableMarkets = (id: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.GET(`api/markets/seller/${id}`)
        .then((res: any) => {
            dispatch(shiftAction.setAvailableMarkets(res.data));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Получить активную сессию смены.
 * 
 * @param userId
 */
export const requestActiveShift = (userId: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.GET(`api/shifts/active/${userId}`)
        .then((res: any) => {
            dispatch(shiftAction.setActiveShift(res.data));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Создать новую смену.
 * 
 * @param marketId
 */
export const createNewShift = (marketId: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.POST('api/shifts', {
        marketId,
        sellerId: store.getState().users.currentUser?.id
    })
        .then((res: any) => {
            dispatch(shiftAction.setActiveShift(res.data));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Получить данные рабочей точки для смены.
 * 
 * @param marketId
 */
export const requestMarketShift = (marketId: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.GET(`api/markets/${marketId}`)
        .then((res: any) => {
            dispatch(shiftAction.setCurrentMarketShift(res.data));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Получить список операций.
 * 
 * @param shiftId
 */
export const requestShiftOrders = (shiftId: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.GET(`api/orders/${shiftId}`)
        .then((res: any) => {
            dispatch(shiftAction.setOrdersHistory(res.data));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Отправить чек.
 * 
 * @param data
 */
export const sendOrder = (data: any): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.POST('api/orders', data)
        .then(() => {
            dispatch(requestShiftOrders(store.getState().shift.activeShift.id));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Запрос на закрытие смены.
 * 
 * @param shiftId
 */
export const closeShift = (shiftId: number): ThunkType => (dispatch): any => {
    dispatch(shiftAction.toggleLoading(true));
    API.POST('api/reports', { shiftId })
        .then(() => {
            const userId = store.getState().users.currentUser?.id;
            userId && dispatch(requestActiveShift(userId));
            dispatch(shiftAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(shiftAction.toggleLoading(false));
        });
};

/** Запросить список отчетов. */
export const requestReports = (): ThunkType => (dispatch): any => {
    API.GET('api/reports')
        .then((res: any) => {
            dispatch(shiftAction.setReportsList(res.data));
        });
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof shiftAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
