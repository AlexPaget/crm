import { FormControl, InputLabel, Select } from '@material-ui/core';
import { EUserRole } from 'Consts/Consts';
import { IRegionModel, IUserModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory, NavLink } from 'react-router-dom';
import { regionsAction, requestRegionById, setRegionManager } from 'Store/Regions/RegionsReducer';
import { TStore } from 'Store/Store';
import { requestUsersByRole } from 'Store/Users/UsersReducer';

/** Карточка региона.
 *
 * @param props
 */
export const RegionCard = (): JSX.Element => {
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Модель региона. */
    const regionCard = useSelector<TStore, IRegionModel | null>(store => store.regions.regionCard);
    /** Список менеджеров. */
    const managersList = useSelector<TStore, IUserModel[]>(store => store.users.usersList);

    /** Режим изменения управляющего. */
    const [isEditManager, setIsEditManager] = useState<boolean>(false);
    /** Выбранный менеджер */
    const [
        selectedManagerId,
        setSelectedManagerId
    ] = useState<number>(regionCard?.manager?.id ?? -1);

    useEffect(() => {
        dispatch(requestRegionById(id));
        dispatch(requestUsersByRole(EUserRole.REGION_MANAGER));

        return (): void => {
            dispatch(regionsAction.clearRegionCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };

    /** Обработчик кнопки выбора менеджера. */
    const handleSelectManager = (): void => {
        const managerId = selectedManagerId === -1 ? null : selectedManagerId;
        const params: any = {
            regionId: id,
            managerId: managerId,
        };
        dispatch(setRegionManager(params));
        setIsEditManager(false);
    };

    /** Рендер списка городов. */
    const renderCities = (): JSX.Element => (
        <div>
            <p>города:</p>
            {regionCard?.cities?.map((item) => (
                <NavLink
                    key={`city-${item.id}`}
                    className="navlink--text"
                    to={`/cities/${item.id}`}
                >
                    {item.name}
                </NavLink>
            ))
            }
        </div>
    );

    /** Рендер менеджера. */
    const renderManagerData = (): JSX.Element => (
        <div>
            <span>Управляющий: </span>
            {regionCard?.manager?.userName
                ? <NavLink
                    className="navlink--text"
                    to={`/users/${regionCard.manager.id}`}
                >
                    {regionCard.manager.userName}
                </NavLink>
                : <span>не назначен</span>}
            <button onClick={(): void => setIsEditManager(true)}>Изменить</button>
        </div>
    );

    /** Рендер селекта выбора управляющего. */
    const renderManagerSelect = (): JSX.Element => (
        <FormControl>
            <InputLabel htmlFor="region-select">Выбрать менеджера</InputLabel>
            <Select
                native
                value={selectedManagerId}
                onChange={(e): void => setSelectedManagerId(e.target.value as number)}
                inputProps={{
                    name: 'manager',
                    id: 'manager-select',
                }}>
                <option aria-label="None" value={-1}>не выбран</option>
                {managersList?.map((m) => (
                    <option key={`manager-${m.id}`} value={m.id}>{m.userName}</option>
                ))}
            </Select>
            <button onClick={handleSelectManager}>Выбрать</button>
        </FormControl>
    );

    return (
        <div>
            <button onClick={handleBackButtonPress}>назад</button>
            {regionCard && <h2>{regionCard?.name}</h2>}
            {renderCities()}
            {isEditManager ? renderManagerSelect() : renderManagerData()}
        </div>
    );
};
