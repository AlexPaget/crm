import { API } from 'Api/Api';
import { IMarketModel, IUserModel } from 'Models/Models';
import { requestAvailableProducts } from 'Store/Products/ProductsReducer';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - MarketsReducer. */
export enum EMarketsAction {
    /** Добавить точку. */
    ADD_MARKET = 'markets/ADD_MARKET',
    /** Добавить карточку точки. */
    SET_MARKET_CARD = 'markets/SET_MARKET_CARD',
    /** Очистить карточку. */
    CLEAR_MARKET_CARD = 'markets/CLEAR_MARKET_CARD',
    /** Добавить список городов. */
    ADD_MARKETS_LIST = 'markets/ADD_MARKETS_LIST',
    /** Добавить список доступных продавцов. */
    SET_AVAILABLE_SELLERS = 'markets/SET_AVAILABLE_SELLERS',
    /** Переключение индикатора загрузки городов. */
    TOGGLE_LOADING = 'markets/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Список точек. */
    marketsList: IMarketModel[];
    /** Карточка точки. */
    marketCard: IMarketModel | null;
    /** Список доступных продавцов. */
    availableSellers: IUserModel[];
    /** Индикатор загрузки точек. */
    isLoading: boolean;
}

const initialState: IState = {
    marketsList: [],
    marketCard: null,
    availableSellers: [],
    isLoading: false
};

/** Редюсер торговых точек.
 * 
 * @param state
 * @param action 
 */
export const marketsReducer = (state = initialState, action: TReducerAction<typeof marketsAction>): IState => {
    switch (action.type) {
        case (EMarketsAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.value };
        case (EMarketsAction.SET_MARKET_CARD):
            return { ...state, marketCard: action.markedCard };
        case (EMarketsAction.CLEAR_MARKET_CARD):
            return { ...state, marketCard: null };
        case (EMarketsAction.ADD_MARKET):
            return {
                ...state,
                marketsList: [
                    ...state.marketsList,
                    action.market
                ]
            };
        case (EMarketsAction.ADD_MARKETS_LIST):
            return {
                ...state,
                marketsList: action.marketsList
            };
        case (EMarketsAction.SET_AVAILABLE_SELLERS):
            return {
                ...state,
                availableSellers: action.availableSellers
            };
        default:
            return state;
    }
};

export const marketsAction = {
    /** Добавить точку.
     * 
     * @param market Торговая точка.
     */
    addMarket: (market: IMarketModel) => ({ type: EMarketsAction.ADD_MARKET, market} as const),
    /** Добавить карточку точки.
     * 
     * @param markedCard
     */
    setMarketCard: (markedCard: IMarketModel) => (
        { type: EMarketsAction.SET_MARKET_CARD, markedCard } as const
    ),
    /** Очистить карточку точки. */
    clearMarketCard: () => ({ type: EMarketsAction.CLEAR_MARKET_CARD } as const),
    /** Добавить список точек
     * 
     * @param marketsList
     */
    addMarketsList: (marketsList: IMarketModel[]) => (
        { type: EMarketsAction.ADD_MARKETS_LIST, marketsList } as const
    ),
    /** Добавить список доступных продавцов.
     * 
     * @param availableSellers
     */
    setAvailableSellers: (availableSellers: IUserModel[]) => (
        { type: EMarketsAction.SET_AVAILABLE_SELLERS, availableSellers } as const
    ),
    /** Переключить индикатор загрузки торговых точек.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EMarketsAction.TOGGLE_LOADING, value } as const
    )
};

/** Создать новую точку.
 * 
 * @param data
 */
export const createNewMarket = (data: any): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.POST('api/markets', data)
        .then(() => {
            /** Обновить список точек */
            dispatch(requestAllMarkets());
        })
        .catch(() => dispatch(marketsAction.toggleLoading(false)));
};

/** Получить точку по ID.
 * 
 * @param id
 */
export const requestMarketById = (id: number): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.GET(`api/markets/${id}`)
        .then((res: any) => {
            dispatch(marketsAction.setMarketCard(res.data));
            dispatch(marketsAction.toggleLoading(false));
        })
        .catch(() => dispatch(marketsAction.toggleLoading(false)));
};

/** Получить список торговых точек */
export const requestAllMarkets = (): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.GET('api/markets')
        .then((res: any) => {
            dispatch(marketsAction.addMarketsList(res.data));
            dispatch(marketsAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Модель данных для добавления товара. */
interface IAddProductParams {
    /** ИД точки */
    marketId: number;
    /** ИД товара */
    productId: number;
}
/** Добавить товар на точку.
 * 
 * @param params
 */
export const addProductToMarket = (params: IAddProductParams): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.POST('api/markets/product', params)
        .then(() => {
            dispatch(requestMarketById(params.marketId));
            dispatch(requestAvailableProducts(params.marketId));
            dispatch(marketsAction.toggleLoading(false));
        })
        .then(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Удалить товар из точки.
 * 
 * @param params
 */
export const removeProductFromMarket = (params: IAddProductParams): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.POST('api/markets/productremove/', params)
        .then(() => {
            dispatch(requestMarketById(params.marketId));
            dispatch(requestAvailableProducts(params.marketId));
            dispatch(marketsAction.toggleLoading(false));
        })
        .then(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Модель данных для изменения товара. */
interface IUpdateProductData extends IAddProductParams {
    /** Цена. */
    price?: number;
    /** Количество. */
    count?: number;
}
/** Изменить инфо о товаре.
 *
 * @param data
 */
export const updateProductFromMarket = (
    data: IUpdateProductData
): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.PUT(
        `api/markets/${data.marketId}/product/${data.productId}`,
        { price: data.price, count: data.count }
    )
        .then(() => {
            dispatch(requestMarketById(data.marketId));
            dispatch(marketsAction.toggleLoading(false));
        })
        .then(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Получить список доступных продавцов.
 * 
 * @param marketId
 */
export const requestAvailableSellers = (marketId: number): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.GET(`api/markets/availablesellers/${marketId}`)
        .then((res: any) => {
            dispatch(marketsAction.setAvailableSellers(res.data));
            dispatch(marketsAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Модель данных для добавления товара. */
interface IAddSellerParams {
    /** ИД точки */
    marketId: number;
    /** ИД продавца */
    sellerId: number;
}
/** Добавить продавца на точку.
 * 
 * @param params
 */
export const addSellerToMarket = (params: IAddSellerParams): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.POST('api/markets/seller', params)
        .then(() => {
            dispatch(requestMarketById(params.marketId));
            dispatch(requestAvailableSellers(params.marketId));
            dispatch(marketsAction.toggleLoading(false));
        })
        .then(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};
/** Убрать продавца из точки.
 * 
 * @param params
 */
export const removeSellerFromMarket = (params: IAddSellerParams): ThunkType => (dispatch): any => {
    dispatch(marketsAction.toggleLoading(true));
    API.DELETE(`api/markets/${params.marketId}/seller/${params.sellerId}`)
        .then(() => {
            dispatch(requestMarketById(params.marketId));
            dispatch(requestAvailableProducts(params.marketId));
            dispatch(marketsAction.toggleLoading(false));
        })
        .then(() => {
            dispatch(marketsAction.toggleLoading(false));
        });
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof marketsAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
