import {
    Button,
    Popover,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography
} from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import AddIcon from '@material-ui/icons/Add';
import './sellersBlock.scss';
import { useParams } from 'react-router-dom';
import { IUserModel } from 'Models/Models';
import {
    addSellerToMarket,
    removeSellerFromMarket,
    requestAvailableSellers
} from 'Store/Markets/MarketsReducer';
import { SellerItem } from './SellerItem';

/** Свойства компонента. */
interface IProps {
    /** Продавцы. */
    sellers?: IUserModel[];
}

/** Компонент блока продавцов на рабочей точке.
 * 
 * @param props Свойства компонента
 */
export const SellersBlock = (props: IProps): JSX.Element => {
    const { sellers } = props;

    const { id } = useParams<any>();
    const dispatch = useDispatch();

    /** Список доступных для добавления продавцов. */
    const availableSellers =
        useSelector<TStore, IUserModel[]>(store => store.markets.availableSellers);

    const [anchorEl, setAnchorEl] = useState(null);

    useEffect(() => {
        dispatch(requestAvailableSellers(id));
    }, []);

    /** Обработчик открытия поповера.
     * 
     * @param event
     */
    const handlePopoverClick = (event: any): void => {
        event?.currentTarget && setAnchorEl(event.currentTarget);
    };

    /** Обработчик закрытия поповера. */
    const handlePopoverClose = (): void => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const popoverId = open ? 'simple-popover' : undefined;

    /** Обработчик удаления продавца.
     * 
     * @param sellerId
     */
    const handleRemoveSeller = (sellerId: number): void => {
        dispatch(removeSellerFromMarket({
            marketId: id,
            sellerId
        }));
    };

    /** Обработчик добавления продавца.
     * 
     * @param sellerId
     */
    const handleAddSeller = (sellerId: number): void => {
        dispatch(addSellerToMarket({
            marketId: id,
            sellerId
        }));
    };

    /** Рендер доступных продавцов. */
    const renderAvailableSellers = (): JSX.Element => (
        <div className="sellers-block__available-sellers-list">
            {availableSellers?.map((item) => (
                <div
                    key={item.id}
                    className="sellers-block__available-item"
                    onClick={(): void => handleAddSeller(item.id)}
                >
                    <AddIcon className="sellers-block__add-icon" />
                    <span>{item.userName}</span>
                </div>
            ))}
        </div>
    );

    /** Рендер продавцов. */
    const renderSellers = (): JSX.Element | JSX.Element[] => (
        <Table aria-label="customized table">
            <TableHead className="sellers-block__table-head">
                <TableRow>
                    <TableCell size="small">Имя</TableCell>
                    <TableCell />
                </TableRow>
            </TableHead>
            <TableBody>
                {sellers?.map((s) => (
                    <SellerItem
                        id={s.id}
                        key={`seller-item-${s.id}`}
                        name={s.userName}
                        onRemove={handleRemoveSeller}
                    />
                ))}
            </TableBody>
        </Table>
    );

    return (
        <div className="sellers-block">
            <div className="sellers-block__header">
                <p className="sellers-block__title">Продавцы</p>
                <div>
                    <Button
                        aria-describedby={id}
                        variant="contained"
                        onClick={handlePopoverClick}
                    >
                        Добавить
                    </Button>
                    <Popover
                        id={popoverId}
                        open={open}
                        anchorEl={anchorEl}
                        onClose={handlePopoverClose}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                    >
                        {availableSellers.length
                            ? renderAvailableSellers()
                            : <Typography>Нет доступных продавцов</Typography>}
                    </Popover>
                </div>
            </div>
            <div className="sellers-block__list">
                {sellers?.length
                    ? renderSellers()
                    : <Typography
                        align="center"
                        className="products-block__empty"
                    >нет добавленных продавцов</Typography>
                }
            </div>
        </div>
    );
};
