import { Button, Checkbox, TextField } from '@material-ui/core';
import Stack from '@mui/material/Stack';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import './reports.scss';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { IUserModel } from 'Models/Models';
import { createRegionReport, requestRawReportsForRegion } from 'Store/Reports/ReportsReducer';
import { format } from 'date-fns';

/** Компонент создания регионального отчета. */
export const CreateRegionReportForm = (): JSX.Element => {
    const dispatch = useDispatch();
    const history = useHistory();

    const user = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);
    /** Отчеты. */
    const reports = useSelector<TStore, any[]>(store => store.report.rawReportsForRegion);
    const [date, setDate] = useState<Date>(new Date());

    /** Расходы. */
    const [expenses, setExpenses] = useState<number>(0);
    /** Включенные смены. */
    const [includeReports, setIncludeReports] = useState<Set<number>>(new Set());

    useEffect(() => {
        if (user?.managedRegionId && date) {
            dispatch(
                requestRawReportsForRegion(user.managedRegionId, format(date, 'yyyy-MM-dd'))
            );
        }
    }, [user, date]);

    /** Обработчик отправки отчета. */
    const handleSendReport = (): void => {
        user?.managedRegionId && includeReports.size > 0 && dispatch(createRegionReport({
            regionId: user?.managedRegionId,
            reportIds: [...includeReports],
            expenses,
            date: format(date, 'yyyy-MM-dd')
        }));
        setExpenses(0);
    };

    /** Обработчик переключения добавления смены в отчет.
     * 
     * @param reportId ИД смены
     */
    const toggleIncludeReport = (reportId: number): void => {
        setIncludeReports((prev) => {
            const newState = new Set([...prev]);
            newState.has(reportId)
                ? newState.delete(reportId)
                : newState.add(reportId);

            return newState;
        });
    };

    /** Рендер списка закрытых смен. */
    const renderShifts = (): JSX.Element => (
        <div>
            <p>включить в отчет:</p>
            {reports && reports.map((r) => (
                <div
                    key={`order-${r.id}`}
                    className={`
                        reports__shift-item
                        ${includeReports.has(r.id) ? 'reports__shift-item--active' : ''}
                    `}
                >
                    <Checkbox
                        value={includeReports.has(r.id)}
                        onClick={(): void => toggleIncludeReport(r.id)}
                    />
                    <strong><span className="reports__shift-item--elem">{r.city.name}</span></strong>
                    <div>
                        <span className="reports__shift-item--elem">выручка: {r.total}</span>
                        <span className="reports__shift-item--link">подробнее</span>
                    </div>
                </div>
            ))}
        </div>
    );

    return (
        <div>
            <Button
                size="small"
                variant="outlined"
                onClick={history.goBack}
            >
                назад
            </Button>
            <h2>Создание отчета</h2>
            <div className="reports__create">
                <LocalizationProvider dateAdapter={AdapterDateFns}>
                    <Stack spacing={3}>
                        <DesktopDatePicker
                            label="Выберите дату"
                            inputFormat="yyyy/MM/dd"
                            value={date}
                            onChange={(newDate: any): void => setDate(newDate)}
                            renderInput={(params: any): JSX.Element => <TextField {...params} />}
                        />
                        {renderShifts()}
                        <TextField
                            variant="outlined"
                            label="Расходы"
                            type="number"
                            value={expenses}
                            onChange={({ target }): void => setExpenses(Number(target.value))}
                        />
                    </Stack>
                </LocalizationProvider>
            </div>
            <Button
                variant="contained"
                color="primary"
                onClick={handleSendReport}
            >
                Создать
            </Button>
        </div>
    );
};
