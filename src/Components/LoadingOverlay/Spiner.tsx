import { CircularProgress } from '@material-ui/core';
import './loadingOverlay.css';

/** Компонент спинера подгрузки данных. */
export const Spinner = (): JSX.Element => (
    <div className="spinner-container">
        <CircularProgress color='primary' />
    </div>
);
