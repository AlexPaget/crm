import { IUserModel } from 'Models/Models';
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { requestAllUsers } from 'Store/Users/UsersReducer';
import { Route, Switch, useRouteMatch, useHistory } from 'react-router-dom';
import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { roleNameConf } from 'Utils/Utils';
import { EUserRole } from 'Consts/Consts';
import { UserCard } from './UserCard/UserCard';
import './workersContent.scss';

/** Модель свойств компонента. */
interface TableItemProps {
    /** Идентификатор. */
    id: number;
    /** Имя */
    name: string;
    /** Город */
    city?: string;
    /** Должность */
    role?: EUserRole;
}

/** Компонент страницы сотрудников. */
export const WorkersContent: FC = (): JSX.Element => {
    const { path } = useRouteMatch();
    const history = useHistory();
    const dispatch = useDispatch();
    const classes = useStyles();

    /** Список пользователей. */
    const usersList = useSelector<TStore, IUserModel[]>(store => store.users.usersList);

    useEffect(() => {
        dispatch(requestAllUsers());
    }, []);

    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/users/${id}`);
    };

    /** Рендер списка пользователей. */
    const renderUsesList = (): JSX.Element => (
        <div className="workers-content__table-block">
            <h2>Сотрудники</h2>
            <TableContainer component={Paper}>
                <Table size="small" aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Имя</TableCell>
                            <TableCell align="center" className={classes.headerCell}>Должность</TableCell>
                            <TableCell align="center" className={classes.headerCell}>Город</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {usersList && usersList?.map((u) => (
                            renderTableElement({
                                id: u.id,
                                name: u.userName,
                                city: u.city,
                                role: u.account?.role,
                            })
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );

    /** Рендер элемента таблицы.
     *
     * @param data Параметры.
     */
    const renderTableElement = (data: TableItemProps): JSX.Element => (
        <TableRow key={`user-item-${data.id}`}>
            <TableCell
                component="th"
                scope="row"
                className="workers-content__user-href"
                onClick={(): void => handleItemClick(data.id)}
            >{data.name ?? ''}</TableCell>
            <TableCell align="center" component="th" scope="row">
                {data.role ? roleNameConf[data.role] : ''}
            </TableCell>
            <TableCell align="center" component="th" scope="row">{data.city ?? ''}</TableCell>
        </TableRow>
    );

    return (
        <div>
            <Switch>
                <Route exact path={path}>
                    {renderUsesList()}
                </Route>
                <Route path={`${path}/:id`}>
                    <UserCard />
                </Route>
            </Switch>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
