import './App.css';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Admin } from 'Pages/Admin/Admin';
import { Auth } from 'Pages/Auth/Auth';
import { Manager } from 'Pages/Manager/Manager';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AuthService } from 'Services/AuthService';
import { TStore } from 'Store/Store';
import { IAuthData } from 'Models/Models';
import { authAction } from 'Store/Auth/AuthReducer';
import { EUserRole } from 'Consts/Consts';
import { Seller } from 'Pages/Seller/Seller';

/** Корневой конпонент приложения. */
function App(): JSX.Element {

    const dispatch = useDispatch();

    const authData = useSelector<TStore, IAuthData | null>(store => store.auth.data);

    useEffect(() => {
        dispatch(authAction.setAuthData(AuthService.getAuth()));
    }, []);

    /** Получить роут в зависимости от роли пользователя. */
    const getCabinetRoute = {
        [EUserRole.SELLER]: <Seller />,
        [EUserRole.CITY_MANAGER]: <Manager />,
        [EUserRole.REGION_MANAGER]: <Manager />,
        [EUserRole.GENERAL_MANAGER]: <Manager />,
        [EUserRole.ADMIN]: <Admin />,
    };

    return (
        <Switch>
            <Route path="/admin" component={(): JSX.Element => <Admin />} />
            <Route path="/auth" component={(): JSX.Element => (
                authData ? <Redirect to="/" /> : <Auth />
            )} />
            <Route path="/">
                {authData ? getCabinetRoute[authData?.role] : <Auth />}
            </Route>
        </Switch>
    );
}

export default App;
