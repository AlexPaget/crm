import { useEffect, useState } from 'react';
import {
    Button,
    Container,
    CssBaseline,
    FormControl,
    InputLabel,
    makeStyles,
    Select,
    TextField,
    Typography
} from '@material-ui/core';
import { AuthService } from 'Services/AuthService';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { ICityModel } from 'Models/Models';
import { requestAllCities } from 'Store/Cities/CitiesReducer';

/** Тип формы авторизации. */
enum ELoginFormType {
    /** Вход */
    LOGIN = 'LOGIN',
    /** Подтверждение аккаунта. */
    CONFIRM = 'CONFIRM'
}

/** Компонент страницы авторизации. */
export const Auth = (): JSX.Element => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    /** Список доступных городов. */
    const cities = useSelector<TStore, ICityModel[]>(store => store.cities.citiesList);

    /** Значение поля ввода Логина. */
    const [login, setLogin] = useState<string>('');
    /** Имя. */
    const [name, setName] = useState<string>('');
    /** Значение поля ввода пароля. */
    const [password, setPassword] = useState<string>('');
    /** Поддверждение пароля. */
    const [confirmPassword, setConfirmPassword] = useState<string>('');
    /** Город пользователя. */
    const [city, setCity] = useState<string>('');
    /** Тип формы. */
    const [formType, setFormType] = useState<ELoginFormType>(ELoginFormType.LOGIN);

    useEffect(() => {
        dispatch(requestAllCities());
    },[]);

    /** Обработчик клина на кнопку отправить. */
    const handleButtonPress = (): void => {
        if (login.length && password.length) {
            formType === ELoginFormType.LOGIN && AuthService.login({
                login: login,
                password: password,
            }, () => history.push('/'));

            if (formType === ELoginFormType.CONFIRM) {
                checkPasswords()
                    ? AuthService.confirm({
                        login: login,
                        password: password,
                        userName: name,
                        city: city
                    }, () => history.push('/'))
                    : alert('Пароли не совпадают');
            }
        }
    };

    /** Сравнить пароли. */
    const checkPasswords = (): boolean => (password === confirmPassword);

    /** Обработчик переклчения типа формы. */
    const handleChangeFormType = (): void => {
        setFormType((prev) => (
            prev === ELoginFormType.LOGIN
                ? ELoginFormType.CONFIRM
                : ELoginFormType.LOGIN
        ));
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    PhotoTime CRM
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="login"
                        label="Логин"
                        name="login"
                        onChange={(e): void => setLogin(e.target.value)}
                        autoFocus
                    />
                    {formType === ELoginFormType.CONFIRM &&
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="userName"
                            label="Имя"
                            name="userName"
                            onChange={(e): void => setName(e.target.value)}
                        />
                    }
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Пароль"
                        type="password"
                        id="password"
                        onChange={(e): void => setPassword(e.target.value)}
                        autoComplete="current-password"
                    />
                    {formType === ELoginFormType.CONFIRM &&
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Подтвердить пароль"
                            type="password"
                            id="password-confirm"
                            onChange={(e): void => setConfirmPassword(e.target.value)}
                            autoComplete="confirm-password"
                        />
                    }
                    {formType === ELoginFormType.CONFIRM &&

            <FormControl>
                <InputLabel htmlFor="region-select">Город</InputLabel>
                <Select
                    native
                    value={city}
                    onChange={(e): void => setCity(e.target.value as string)}
                    inputProps={{
                        name: 'city',
                        id: 'city-select',
                    }}>
                    <option aria-label="None" value={-1}>не выбран</option>
                    {cities?.map((c) => (
                        <option key={`city-${c.id}`} value={c.name}>{c.name}</option>
                    ))}
                </Select>
            </FormControl>
                    }
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleButtonPress}
                    >
                        {formType === ELoginFormType.LOGIN ? 'Войти' : 'Подтвердить'}
                    </Button>
                </form>
                <span className={classes.switchButton} onClick={handleChangeFormType}>
                    {formType === ELoginFormType.LOGIN ? 'Подтвердить' : 'Войти'}
                </span>
            </div>
        </Container>
    );
};

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    switchButton: {
        alignSelf: 'flex-end',
        color: theme.palette.primary.main,
        padding: theme.spacing(0, 1),
        cursor: 'pointer',
    }
}));
