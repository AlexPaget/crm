import { Button } from '@material-ui/core';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { closeShift, requestShiftOrders } from 'Store/Shift/ShiftReducer';
import { TStore } from 'Store/Store';
import { EPayType } from './WorkShift';

/** Компонент закрытия смены.
 * 
 * @param props
 */
export const ShiftCloseForm = (props: any): JSX.Element => {
    const { onClose } = props;
    const dispatch = useDispatch();

    /** Активная смена. */
    const activeShift =
        useSelector<TStore, any>(store => store.shift.activeShift);
    /** Список операций. */
    const ordersList = useSelector<TStore, any[]>(store => store.shift.ordersList);

    const [isCash, setIsCash] = useState<boolean>(false);
    const [isCashless, setIsCashless] = useState<boolean>(false);
    const [isTransit, setIsTransit] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestShiftOrders(activeShift.id));
    }, []);

    /** Сверка налички. */
    const renderCashCheck = (): JSX.Element => {
        let cash: number = 0;
        ordersList.forEach((el) => {
            if (el.payType === EPayType.CASH) {
                cash += el.totalPrice;
            };
        });

        return (
            <div>
                <h4>Наличными принято</h4>
                <h3>{cash}</h3>
                <Button onClick={(): void => setIsCash(true)}>Подтвердить</Button>
            </div>
        );
    };

    /** Сверка налички. */
    const renderCashlessCheck = (): JSX.Element => {
        let cash: number = 0;
        ordersList.forEach((el) => {
            if (el.payType === EPayType.CASHLESS) {
                cash += el.totalPrice;
            };
        });

        return (
            <div>
                <h4>Безнал принято</h4>
                <h3>{cash}</h3>
                <Button onClick={(): void => setIsCashless(true)}>Подтвердить</Button>
            </div>
        );
    };

    /** Сверка налички. */
    const renderTransitCheck = (): JSX.Element => {
        let cash: number = 0;
        ordersList.forEach((el) => {
            if (el.payType === EPayType.TRANSFER) {
                cash += el.totalPrice;
            };
        });

        return (
            <div>
                <h4>Переводом принято</h4>
                <h3>{cash}</h3>
                <Button onClick={(): void => setIsTransit(true)}>Подтвердить</Button>
            </div>
        );
    };

    /** Обработчик закрытия смены. */
    const handleCloseShift = (): void => {
        dispatch(closeShift(activeShift.id));
        onClose();
    };

    /** Рендер кнопки закрыть. */
    const renderCloseButton = (): JSX.Element => (
        <Button onClick={handleCloseShift}>Закрыть смену</Button>
    );

    /** Форма сверки денег. */
    const renderMoneyCheck = (): JSX.Element => {
        if (!isCash) {
            return renderCashCheck();
        } else if (!isCashless) {
            return renderCashlessCheck();
        } else if (!isTransit) {
            return renderTransitCheck();
        } else {
            return renderCloseButton();
        }
    };

    return (
        <div className="close-shift__dialog">
            <h5>Закрыть смену</h5>
            {renderMoneyCheck()}
        </div>
    );
};
