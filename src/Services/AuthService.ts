import { API } from 'Api/Api';
import jwt_decode from 'jwt-decode';
import { IAuthData } from 'Models/Models';
import { authAction } from 'Store/Auth/AuthReducer';
import { store } from 'Store/Store';

/** Модель данных для логина. */
interface ILoginData {
    /** Логин. */
    login: string;
    /** Пароль. */
    password: string;
}

/** Модель данных для логина. */
interface IConfirmData {
    /** Логин. */
    login: string;
    /** Имя. */
    userName: string;
    /** Пароль. */
    password: string;
    /** Идентификатор города. */
    city: string;
}

/** Сервис авторизации. */
export class AuthService {
    /** Авторизоваться.
     * 
     * @param data Данные для авторизации.
     * @param callback
     */
    public static login = (data: ILoginData, callback?: () => void): void => {
        API.POST('auth/login', {
            login: data.login,
            password: data.password,
        })
            .then((res: any) => {
                AuthService.setCookie('AuthToken', JSON.stringify(res.data.token), 30);
                store.dispatch(authAction.setAuthData(AuthService.getAuth()));
                callback && callback();
            });
    };
    /** Выйти из аккаунта.
     * 
     * @param callback
     */
    public static logout = (callback?: () => void): void => {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i];
            const eqPos = cookie.indexOf('=');
            const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT`;
        }
        store.dispatch(authAction.clearAuthData());
        callback && callback();
    };

    /** Подтвердить аккаунт.
     *
     * @param data Данные для подтверждения.
     * @param callback
     */
    public static confirm = (data: IConfirmData, callback?: () => void): void => {
        API.POST('auth/confirm', {
            login: data.login,
            password: data.password,
            userName: data.userName,
            city: data.city
        })
            .then((res: any) => {
                AuthService.setCookie('AuthToken', JSON.stringify(res.data.token), 30);
                store.dispatch(authAction.setAuthData(AuthService.getAuth()));
                callback && callback();
            });
    };

    /** Проверить авторизацию. */
    public static getAuth = (): IAuthData | null => {
        const token = AuthService.getCookie('AuthToken');
        const authData = token ? jwt_decode<IAuthData>(token) : null;

        return authData;
    };

    /** Записать cookie.
     * 
     * @param name
     * @param val
     * @param expires
     */
    static setCookie(name: string, val: string, expires: number): void {
        const date = new Date();
        date.setDate(date.getDate() + expires);
        document.cookie = `${name}=${val}; path=/; expires=${date.toUTCString()}`;
    }
    /** Получить cookie
     * 
     * @param name
     */
    static getCookie(name: string): string | null {
        const matches = document.cookie.match(new RegExp(
            `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')}=([^;]*)`
        ));

        return matches ? decodeURIComponent(matches[1]) : null;
    }
};
