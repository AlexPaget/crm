import { Action, applyMiddleware, combineReducers, createStore } from 'redux';
import thunk, { ThunkAction } from 'redux-thunk';
import { accountsReducer } from './Accounts/AccountsReducer';
import { authReducer } from './Auth/AuthReducer';
import { citiesReducer } from './Cities/CitiesReducer';
import { marketsReducer } from './Markets/MarketsReducer';
import { productReducer } from './Products/ProductsReducer';
import { regionsReducer } from './Regions/RegionsReducer';
import { reportsReducer } from './Reports/ReportsReducer';
import { shiftReducer } from './Shift/ShiftReducer';
import { usersReducer } from './Users/UsersReducer';

/** Корневой редюсер. */
const rootReducer = combineReducers({
    auth: authReducer,
    users: usersReducer,
    cities: citiesReducer,
    regions: regionsReducer,
    markets: marketsReducer,
    accounts: accountsReducer,
    products: productReducer,
    shift: shiftReducer,
    report: reportsReducer
});

/** Инстанс главного хранилища */
export const store = createStore(rootReducer, applyMiddleware(thunk));

/** Тип корневого стора. */
export type TStore = ReturnType<typeof rootReducer>;
/** Тип экшенов. */
export type TReducerAction<T> = T extends { [keys: string]: (...args: any[]) => infer U } ? U : never;
/** Тип санок. */
export type BaseThunkType<A extends Action = Action, R = Promise<void>> = ThunkAction<R, TStore, unknown, A>;
