import { Route, Switch } from 'react-router-dom';
import { CitiesContent } from './Content/CitiesContent/CitiesContent';
import { MainContent } from './Content/MainContent.tsx/MainContent';
import { PointsContent } from './Content/PointsContent/PointsContent';
import { RegionsContent } from './Content/RegionsContent.tsx/RegionsContent';
import { ReportsContent } from './Content/ReportsContent/ReportsContent';
import { WorkersContent } from './Content/WorkersContent/WorkersContent';

/** Страницы контента кабинета менеджера. */
export enum EManagerPage {
    /** Главная */
    MAIN = 'MAIN',
    /** Сотрудники. */
    WORKERS = 'WORKERS',
    /** Точки */
    POINTS = 'POINTS',
    /** Города. */
    CITIES = 'CITIES',
    /** Регионы. */
    REGIONS = 'REGIONS',
    /** Отчеты. */
    REPORTS = 'REPORTS'
}
/** Компонент контента кабинета менеджера. */
export const ManagerContent = (): JSX.Element => (
    <Route>
        <Switch>
            <Route path="/dashboard" component={(): JSX.Element => <MainContent />} />
            <Route path="/users" component={(): JSX.Element => <WorkersContent />} />
            <Route path="/points" component={(): JSX.Element => <PointsContent />} />
            <Route path="/cities" component={(): JSX.Element => <CitiesContent />} />
            <Route path="/regions" component={(): JSX.Element => <RegionsContent />} />
            <Route path="/reports" component={(): JSX.Element => <ReportsContent />} />
        </Switch>
    </Route>
);
