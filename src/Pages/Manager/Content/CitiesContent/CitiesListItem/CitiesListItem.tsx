import { IUserModel } from 'Models/Models';
import './citiesListItem.scss';

/** Модель свойст компонента. */
interface IProps {
    /** Идентификатор. */
    id: number;
    /** Название. */
    title: string;
    /** Колличество точек. */
    pointsCount?: number;
    /** Управляющий. */
    manager?: IUserModel;
    /** Коллбек клика */
    onClick?: (id: number) => void;
}

/** Компонент элемента списка городов.
 * 
 * @param props
 */
export const CitiesListItem = (props: IProps): JSX.Element => {
    const { id, title, pointsCount, manager, onClick } = props;

    /** Обработчик клика по элементу. */
    const handleItemLick = (): void => {
        onClick && onClick(id);
    };

    return (
        <div className="cities-list-item" onClick={handleItemLick}>
            <span className="cities-list-item__title">{title}</span>
            <span>{`к-во точек: ${pointsCount}`}</span>
            <span>{`Управляющий: ${manager ? manager.userName : 'не назначен'}`}</span>
        </div>
    );
};
