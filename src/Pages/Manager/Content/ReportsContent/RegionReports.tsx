import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestLastRegionReports, requestRawReportsForRegion } from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода отчетов для региона. */
export const RegionReports = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();
    /** Необработанные отчеты. */
    const rawReports = useSelector<TStore, any[]>(store => store.report.rawReportsForRegion);
    /** Последние отчеты. */
    const lastReports = useSelector<TStore, any[]>(store => store.report.lastRegionReports);
    /** Текущий пользователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);

    useEffect(() => {
        if (currentUser?.managedRegionId){
            dispatch(requestRawReportsForRegion(currentUser.managedRegionId));
            dispatch(requestLastRegionReports(currentUser.managedRegionId));
        }
    }, [currentUser]);

    /** Рендер необработанных отчетов. */
    const renderRawReports = (): JSX.Element => (
        <div className="start-shift__reports-container">
            <h3>Необработанные  отчеты</h3>
            <TableContainer component={Paper}>
                <Table size="small" aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Дата</TableCell>
                            <TableCell className={classes.headerCell}>Город</TableCell>
                            <TableCell className={classes.headerCell}>Выручка</TableCell>
                            <TableCell className={classes.headerCell}>Расходы</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rawReports && rawReports?.map((r) => (
                            <TableRow key={`city-report-item-${r.id}`}>
                                <TableCell component="th" scope="row">{r.date}</TableCell>
                                <TableCell component="th" scope="row">{r.city.name}</TableCell>
                                <TableCell component="th" scope="row">{r.total}</TableCell>
                                <TableCell component="th" scope="row">{r.expenses}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );

    /** Рендер последних отчетов. */
    const renderLastReports = (): JSX.Element => (
        <div className="start-shift__reports-container">
            <h3>Последние отчеты</h3>
            <TableContainer component={Paper}>
                <Table size="small" aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Дата</TableCell>
                            <TableCell className={classes.headerCell}>Выручка</TableCell>
                            <TableCell className={classes.headerCell}>Расходы</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {lastReports?.map((r) => (
                            <TableRow key={`last-report-item-${r.id}`}>
                                <TableCell component="th" scope="row">{r.date}</TableCell>
                                <TableCell component="th" scope="row">{r.total}</TableCell>
                                <TableCell component="th" scope="row">{r.expenses}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );

    return (
        <div>
            {!!rawReports.length && renderRawReports()}
            {!!lastReports.length && renderLastReports()}
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
