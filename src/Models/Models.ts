import { EUserRole } from 'Consts/Consts';

/** Модель пользователя. */
export interface IUserModel {
    /** Идентификатор. */
    id: number;
    /** Имя. */
    userName: string;
    /** Продавец на точке. */
    marketId?: number;
    /** Модель точки. */
    market: IMarketModel;
    /** Управляющий в городе. */
    managedCityId?: number;
    /** Модель города. */
    cityManagerIn?: ICityModel;
    /** Управляющий регином. */
    managedRegionId?: number;
    /** Модель региона. */
    regionManagerIn?: IRegionModel;
    /** Аккаунт */
    account?: IAuthData;
    /** Город */
    city?: string;
};

/** Модель данных авторизации */
export interface IAuthData {
    /** Идетификатор. */
    id: string;
    /** Логин. */
    login: string;
    /** Роль пользователя. */
    role: EUserRole;
}

/** Модель города. */
export interface ICityModel {
    /** Идентификатор. */
    id: number;
    /** Название. */
    name: string;
    /** Точки в городе. */
    markets: IMarketModel[];
    /** Управлящий */
    manager?: IUserModel;
    /** Регион */
    region?: IRegionModel;
};

/** Модель региона */
export interface IRegionModel {
    /** Идентификатор. */
    id: number;
    /** Название. */
    name: string;
    /** Управляющий. */
    manager?: IUserModel;
    /** Города */
    cities?: ICityModel[];
};

/** Модель торговой точки. */
export interface IMarketModel {
    /** Идентификатор. */
    id: number;
    /** Название. */
    name: string;
    /** Город. */
    city: ICityModel;
    /** Работники. */
    sellers: IUserModel[];
    /** Адрес */
    adress: string;
    /** Товары. */
    products: IProductModel[];
};

/** Модель товара. */
export interface IProductModel {
    /** Идетификатор. */
    id: number;
    /** Название. */
    name: string;
    /** Модель товаров на точках. */
    ProductMarket: IProductMarket;
}

/** Модель таблицы товаров на точках. */
export interface IProductMarket {
    /** Id. */
    id: number;
    /** Цена. */
    price: number;
    /** Колличество. */
    count: number;
}

/** Модель аккаунта пользователя. */
export interface IAccountModel {
    /** Идентификатор. */
    id: number;
    /** Логин. */
    login: string;
    /** Должность. */
    role: EUserRole;
    /** Состояние активации. */
    isActive: boolean;
}
