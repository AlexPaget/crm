import { IRegionModel } from 'Models/Models';
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { Route, Switch, useRouteMatch, useHistory } from 'react-router-dom';
import { requestAllRegions } from 'Store/Regions/RegionsReducer';
import { RegionCard } from './RegionCard/RegionCard';
import { RegionsListItem } from './RegionsListItem/RegionsListItem';

/** Компонент страницы сотрудников. */
export const RegionsContent: FC = (): JSX.Element => {
    const { path } = useRouteMatch();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Список регионов. */
    const regionsList = useSelector<TStore, IRegionModel[]>(store => store.regions.regionsList);

    useEffect(() => {
        dispatch(requestAllRegions());
    }, []);

    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/regions/${id}`);
    };

    /** Рендер списка регионов. */
    const renderRegionsList = (): JSX.Element => (
        <div>
            <h2>Регионы</h2>
            {regionsList.map((item) => (
                <RegionsListItem
                    key={`region-${item.id}`}
                    id={item.id}
                    title={item.name}
                    citiesCount={item.cities?.length ?? 0}
                    manager={item.manager}
                    onClick={handleItemClick}
                />
            ))}
        </div>
    );

    return (
        <div>
            <Switch>
                <Route exact path={path}>
                    {renderRegionsList()}
                </Route>
                <Route path={`${path}/:id`}>
                    <RegionCard />
                </Route>
            </Switch>
        </div>
    );
};
