import { ICityModel } from 'Models/Models';
import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { Route, Switch, useRouteMatch, useHistory } from 'react-router-dom';
import { requestAllCities } from 'Store/Cities/CitiesReducer';
import { CityCard } from './CityCard/CityCard';
import { CitiesListItem } from './CitiesListItem/CitiesListItem';

/** Компонент страницы городов. */
export const CitiesContent: FC = (): JSX.Element => {
    const { path } = useRouteMatch();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Список городов. */
    const citiesList = useSelector<TStore, ICityModel[]>(store => store.cities.citiesList);

    useEffect(() => {
        dispatch(requestAllCities());
    }, []);

    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/cities/${id}`);
    };

    /** Рендер списка городов. */
    const renderCitiesList = (): JSX.Element => (
        <div>
            <h2>Города</h2>
            {citiesList.map((item) => (
                <CitiesListItem
                    key={`city-${item.id}`}
                    id={item.id}
                    title={item.name}
                    pointsCount={item.markets?.length ?? 0}
                    manager={item.manager}
                    onClick={handleItemClick}
                />
            ))}
        </div>
    );

    return (
        <div>
            <Switch>
                <Route exact path={path}>
                    {renderCitiesList()}
                </Route>
                <Route path={`${path}/:id`}>
                    <CityCard />
                </Route>
            </Switch>
        </div>
    );
};
