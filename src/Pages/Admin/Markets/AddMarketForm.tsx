import { Button, FormControl, Input, InputLabel, makeStyles, Select } from '@material-ui/core';
import { ICityModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestAllCities } from 'Store/Cities/CitiesReducer';
import { createNewMarket } from 'Store/Markets/MarketsReducer';
import { TStore } from 'Store/Store';

/** Компонент формы добавления торговой точки. */
export const AddMarketForm = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список доступных регинов. */
    const citiesList = useSelector<TStore, ICityModel[]>(store => store.cities.citiesList);

    /** Название точки. */
    const [name, setName] = useState<string>('');
    /** Идентификатор города. */
    const [cityId, setCityId] = useState<number>(-1);
    /** Адрес торговой точки. */
    const [adress, setAdress] = useState<string>('');

    useEffect(() => {
        dispatch(requestAllCities());
    },[]);

    /** Очистить форму. */
    const clearForn = (): void => {
        setName('');
        setCityId(-1);
        setAdress('');
    };

    /** Обработчик добавления точки. */
    const handleCreateMarket = (): void => {
        const data: any = {
            name: name,
            adress: adress,
            cityId: cityId,
        };
        dispatch(createNewMarket(data));
        clearForn();
    };

    return (
        <div className={classes.container}>
            <Input
                className={classes.input}
                placeholder="Название"
                value={name}
                onChange={(e): void => setName(e.target.value)}
            />
            <FormControl>
                <InputLabel htmlFor="region-select">Город</InputLabel>
                <Select
                    native
                    value={cityId}
                    onChange={(e): void => setCityId(e.target.value as number)}
                    inputProps={{
                        name: 'city',
                        id: 'city-select',
                    }}>
                    <option aria-label="None" value={-1}>не выбран</option>
                    {citiesList?.map((city) => (
                        <option key={`city-${city.id}`} value={city.id}>{city.name}</option>
                    ))}
                </Select>
            </FormControl>
            <Input
                className={classes.input}
                placeholder="Адрес"
                value={adress}
                onChange={(e): void => setAdress(e.target.value)}
            />
            <Button variant="contained" color="secondary" onClick={handleCreateMarket}>Добавить</Button>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    input: {
        marginRight: theme.spacing(3),
    }
}));
