import { useState } from 'react';
import {
    AppBar,
    CssBaseline,
    Divider,
    Drawer,
    Hidden,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
    Toolbar,
    Typography,
    useTheme
} from '@material-ui/core';
import ApartmentIcon from '@material-ui/icons/Apartment';
import ContactsIcon from '@material-ui/icons/Contacts';
import PublicIcon from '@material-ui/icons/Public';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import RoomIcon from '@material-ui/icons/Room';
import { EAdminViewContent } from 'Consts/Consts';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { AuthService } from 'Services/AuthService';
import { useHistory } from 'react-router-dom';
import { Users } from './Users/Users';
import { Cities } from './Cities/Cities';
import { Regions } from './Regions/Regions';
import { Markets } from './Markets/Markets';
import { Accounts } from './Accounts/Accounts';
import { Products } from './Products/Products';

/** Компонент страницы админа. */
export const Admin = (): JSX.Element => {
    const classes = useStyles();
    const theme = useTheme();
    const history = useHistory();
    const [mobileOpen, setMobileOpen] = useState(false);
    /** Тип основного контента. */
    const [contentType, setContentType] = useState<EAdminViewContent>(EAdminViewContent.USERS);

    /** Обработчик переключения вида дровера. */
    const handleDrawerToggle = (): void => setMobileOpen(!mobileOpen);

    /** Рендер контента страницы. */
    const renderPageContent = (): JSX.Element => {
        const renderConf = {
            [EAdminViewContent.ACCOUNTS]: <Accounts />,
            [EAdminViewContent.USERS]: <Users />,
            [EAdminViewContent.CITIES]: <Cities />,
            [EAdminViewContent.REGIONS]: <Regions />,
            [EAdminViewContent.MARKETS]: <Markets />,
            [EAdminViewContent.PRODUCTS]: <Products />,
        };

        return renderConf[contentType];
    };

    /** Обработчик кнопки выхода из аккаунта. */
    const handleLogoutClick = (): void => {
        AuthService.logout(() => history.push('/auth'));
    };

    const drawer = (
        <div>
            <div className={classes.toolbar} />
            <Divider />
            <List>
                <ListItem
                    selected={contentType === EAdminViewContent.USERS}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.USERS)}
                >
                    <ListItemIcon>
                        <ContactsIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Сотрудники'} />
                </ListItem>
                <ListItem
                    selected={contentType === EAdminViewContent.ACCOUNTS}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.ACCOUNTS)}
                >
                    <ListItemIcon>
                        <SupervisorAccountIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Аккаунты'} />
                </ListItem>
                <ListItem
                    selected={contentType === EAdminViewContent.MARKETS}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.MARKETS)}
                >
                    <ListItemIcon>
                        <RoomIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Торговые точки'} />
                </ListItem>
                <ListItem
                    selected={contentType === EAdminViewContent.CITIES}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.CITIES)}
                >
                    <ListItemIcon>
                        <ApartmentIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Города'} />
                </ListItem>
                <ListItem
                    selected={contentType === EAdminViewContent.REGIONS}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.REGIONS)}
                >
                    <ListItemIcon>
                        <PublicIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Регионы'} />
                </ListItem>
                <ListItem
                    selected={contentType === EAdminViewContent.PRODUCTS}
                    button
                    onClick={(): void => setContentType(EAdminViewContent.PRODUCTS)}
                >
                    <ListItemIcon>
                        <PublicIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Товары'} />
                </ListItem>
            </List>
        </div>
    );

    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar className={classes.headerToolbar}>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" noWrap>
                        Admin Panel
                    </Typography>
                    <IconButton
                        color="inherit"
                        edge="start"
                        onClick={handleLogoutClick}
                        className={classes.logoutButton}
                    >
                        <ExitToAppIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <nav className={classes.drawer} aria-label="mailbox folders">
                {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                <Hidden smUp implementation="css">
                    <Drawer
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        variant="permanent"
                        open
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {renderPageContent()}
            </main>
        </div>
    );
};

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    logoutButton: {
        marginRight: theme.spacing(2),
    },
    headerToolbar: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    // Necessary for content to be below app bar.
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));
