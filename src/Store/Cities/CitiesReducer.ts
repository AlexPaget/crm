import { API } from 'Api/Api';
import { ICityModel } from 'Models/Models';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - CitiesReducer. */
export enum ECitiesAction {
    /** Добавить город. */
    ADD_CITY = 'cities/ADD_CITY',
    /** Добавить карточку города. */
    SET_CITY_CARD = 'cities/SET_CITY_CARD',
    /** Очистить карточку города. */
    CLEAR_CITY_CARD = 'cities/CLEAR_CITY_CARD',
    /** Добавить список городов. */
    ADD_CITIES_LIST = 'cities/ADD_CITIES_LIST',
    /** Переключение индикатора загрузки городов. */
    TOGGLE_LOADING = 'cities/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Список городов. */
    citiesList: ICityModel[];
    /** Карточка активности. */
    cityCard: ICityModel | null;
    /** Индикатор загрузки городов. */
    isLoading: boolean;
}

const initialState: IState = {
    citiesList: [],
    cityCard: null,
    isLoading: false
};

/** Редюсер городов.
 * 
 * @param state
 * @param action 
 */
export const citiesReducer = (state = initialState, action: TReducerAction<typeof citiesAction>): IState => {
    switch (action.type) {
        case (ECitiesAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.value };
        case (ECitiesAction.SET_CITY_CARD):
            return { ...state, cityCard: action.cityCard };
        case (ECitiesAction.CLEAR_CITY_CARD): {
            return { ...state, cityCard: null };
        }
        case (ECitiesAction.ADD_CITY):
            return {
                ...state,
                citiesList: [
                    ...state.citiesList,
                    action.city
                ]
            };
        case (ECitiesAction.ADD_CITIES_LIST):
            return {
                ...state,
                citiesList: action.citiesList
            };
        default:
            return state;
    }
};

export const citiesAction = {
    /** Добавить город.
     * 
     * @param city Список пользователей.
     */
    addCity: (city: ICityModel) => ({ type: ECitiesAction.ADD_CITY, city} as const),
    /** Добавить карточку города.
     * 
     * @param cityCard
     */
    setCityCard: (cityCard: ICityModel) => (
        { type: ECitiesAction.SET_CITY_CARD, cityCard } as const
    ),
    /** Очитстить карточку города. */
    clearCityCard: () => ({ type: ECitiesAction.CLEAR_CITY_CARD } as const),
    /** Добавить список городов
     * 
     * @param citiesList
     */
    addCitiesList: (citiesList: ICityModel[]) => ({ type: ECitiesAction.ADD_CITIES_LIST, citiesList } as const),
    /** Переключить индикатор загрузки пользователей.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: ECitiesAction.TOGGLE_LOADING, value } as const
    )
};

/** Создать новый город.
 * 
 * @param data
 */
export const createNewCity = (data: any): ThunkType => (dispatch): any => {
    dispatch(citiesAction.toggleLoading(true));
    API.POST('api/cities', data)
        .then(() => {
            /** Обновить список городов */
            dispatch(requestAllCities());
        })
        .catch(() => dispatch(citiesAction.toggleLoading(false)));
};

/** Получить список городов */
export const requestAllCities = (): ThunkType => (dispatch): any => {
    dispatch(citiesAction.toggleLoading(true));
    API.GET('api/cities')
        .then((res: any) => {
            dispatch(citiesAction.addCitiesList(res.data));
            dispatch(citiesAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(citiesAction.toggleLoading(false));
        });
};

/** Получить город по ID.
 * 
 * @param id
 */
export const requestCityById = (id: number): ThunkType => (dispatch): any => {
    dispatch(citiesAction.toggleLoading(true));
    API.GET(`api/cities/${id}`)
        .then((res: any) => {
            dispatch(citiesAction.setCityCard(res.data));
            dispatch(citiesAction.toggleLoading(false));
        })
        .catch(() => dispatch(citiesAction.toggleLoading(false)));
};

/** Назначить управляющего на город.
 * 
 * @param params
 */
export const setCityManager = (params: any): ThunkType => (dispatch): any => {
    dispatch(citiesAction.toggleLoading(true));
    API.PUT(`api/cities/${params.cityId}`, { managerId: params.managerId })
        .then(() => {
            dispatch(citiesAction.toggleLoading(false));
        })
        .catch(() => dispatch(citiesAction.toggleLoading(false)));
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof citiesAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
