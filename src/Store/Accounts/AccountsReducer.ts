import { API } from 'Api/Api';
import { IAccountModel } from 'Models/Models';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - AccountsReducer. */
export enum EAccountsAction {
    /** Добавить аккаунт. */
    ADD = 'accounts/ADD',
    /** Добавить список аккаунтов. */
    ADD_LIST = 'accounts/ADD_LIST',
    /** Переключение индикатора загрузки. */
    TOGGLE_LOADING = 'accounts/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Список городов. */
    accountsList: IAccountModel[];
    /** Индикатор загрузки. */
    isLoading: boolean;
}

const initialState: IState = {
    accountsList: [],
    isLoading: false
};

/** Редюсер аккаунтов.
 * 
 * @param state
 * @param action 
 */
export const accountsReducer = (state = initialState, action: TReducerAction<typeof accountsAction>): IState => {
    switch (action.type) {
        case (EAccountsAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.value };
        case (EAccountsAction.ADD):
            return {
                ...state,
                accountsList: [
                    ...state.accountsList,
                    action.account
                ]
            };
        case (EAccountsAction.ADD_LIST):
            return {
                ...state,
                accountsList: action.accountsList
            };
        default:
            return state;
    }
};

export const accountsAction = {
    /** Добавить аккаунт.
     * 
     * @param account Аккаунт.
     */
    addAccount: (account: IAccountModel) => ({ type: EAccountsAction.ADD, account } as const),
    /** Добавить список аккаунтов
     * 
     * @param accountsList
     */
    addAccountsList: (accountsList: IAccountModel[]) => ({ type: EAccountsAction.ADD_LIST, accountsList } as const),
    /** Переключить индикатор загрузки.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EAccountsAction.TOGGLE_LOADING, value } as const
    )
};

/** Создать новый аккаунт.
 * 
 * @param data
 */
export const createNewAccount = (data: any): ThunkType => (dispatch): any => {
    dispatch(accountsAction.toggleLoading(true));
    API.POST('api/accounts', data)
        .then(() => {
            /** Обновить список аккаунтов */
            dispatch(requestAllAccounts());
        })
        .catch(() => dispatch(accountsAction.toggleLoading(false)));
};

/** Получить список городов */
export const requestAllAccounts = (): ThunkType => (dispatch): any => {
    dispatch(accountsAction.toggleLoading(true));
    API.GET('api/accounts')
        .then((res: any) => {
            dispatch(accountsAction.addAccountsList(res.data));
            dispatch(accountsAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(accountsAction.toggleLoading(false));
        });
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof accountsAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
