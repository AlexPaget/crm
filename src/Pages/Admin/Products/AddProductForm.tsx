import { Button, Input, makeStyles } from '@material-ui/core';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { createNewProduct } from 'Store/Products/ProductsReducer';

/** Компонент формы добавления нового товара. */
export const AddProductForm = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Название товара. */
    const [name, setName] = useState<string>('');

    /** Очистить форму. */
    const clearForm = (): void => {
        setName('');
    };

    /** Обработчик добавления товара. */
    const handleCreateMarket = (): void => {
        const data: any = {
            name: name,
        };
        dispatch(createNewProduct(data));
        clearForm();
    };

    return (
        <div className={classes.container}>
            <Input
                className={classes.input}
                placeholder="Название"
                value={name}
                onChange={(e): void => setName(e.target.value)}
            />
            <Button variant="contained" color="secondary" onClick={handleCreateMarket}>Добавить</Button>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(1),
    },
    input: {
        marginRight: theme.spacing(3),
    }
}));
