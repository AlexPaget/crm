import { IMarketModel, IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createNewShift, requestAvailableMarkets, requestReports } from 'Store/Shift/ShiftReducer';
import { TStore } from 'Store/Store';
import './startShiftMenu.scss';

/** Компонент меню начала смены. */
export const StartShiftMenu = (): JSX.Element => {
    const dispatch = useDispatch();

    /** Текущий пользователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);
    /** Доступные точки для открытия смены. */
    const availableMarkets =
        useSelector<TStore, IMarketModel[]>(store => store.shift.availableMarkets);
    /** Отчеты. */
    const reports = useSelector<TStore, any[]>(store => store.shift.reportsList);

    useEffect(() => {
        currentUser && dispatch(requestAvailableMarkets(currentUser.id));
        dispatch(requestReports());
    }, [currentUser]);

    /** Обработчик клика на город.
     * 
     * @param marketId
     */
    const handleMarketClick = (marketId: number): void => {
        dispatch(createNewShift(marketId));
    };

    /** Рендер доступных точек. */
    const renderAvailableMarkets = (): JSX.Element[] => (
        availableMarkets.map((m) => (
            <div
                key={`available-market-${m.id}`}
                className="start-shift__market-item"
                onClick={(): void => handleMarketClick(m.id)}
            >
                <h4>{m.name}</h4>
            </div>
        ))
    );

    /** Рендер отчетов. */
    const renderReports = (): JSX.Element => (
        <div className="start-shift__reports-container">
            <h3>Завершенные смены</h3>
            {reports && reports?.map((r) => (
                <div key={`order-${r.id}`} className="start-shift__report-item">
                    <h4>{r.shift.market.name}</h4>
                    <div className="start-shift__report-item-content">
                        <span>{new Date(r.createdAt).toLocaleDateString()}</span>
                        <span>Выручка: {r.total}</span>
                    </div>
                </div>
            ))}
        </div>
    );

    return (
        <div className="start-shift">
            <div className="start-shift__markets-container">
                <h2>Выберите торговую точку для начала смены</h2>
                {availableMarkets.length
                    ? renderAvailableMarkets()
                    : <h3>Нет доступных точек</h3>
                }
            </div>
            {renderReports()}
        </div>
    );
};
