import { FC } from 'react';
import { roleNameConf } from 'Utils/Utils';
import { EUserRole } from 'Consts/Consts';
import './usersListItem.scss';

/** Модель свойств компонента. */
interface IProps {
    /** Идентификатор. */
    id: number;
    /** Имя */
    name: string;
    /** Город */
    city?: string;
    /** Должность */
    role?: EUserRole;
    /** Коллбек клика. */
    onClick?: (id: number) => void;
}

/** Компонент эдемента списка пользователей.
 *
 * @param props
 */
export const UsersListItem: FC<IProps> = (props): JSX.Element => {
    const { id, name, city, role, onClick } = props;

    /** Обработчик клика по элементу. */
    const handleClick = (): void => {
        onClick && onClick(id);
    };

    return (
        <div className="users-list-item" onClick={handleClick}>
            <div>
                <span className="users-list-item__name">{name}</span>
                {role &&
                    <span
                        className="users-list-item__role"
                    >
                        {` (${roleNameConf[role]})`}
                    </span>
                }
            </div>
            <div>
                {city && <span className="users-list-item__city">{city}</span>}
            </div>
        </div>
    );
};
