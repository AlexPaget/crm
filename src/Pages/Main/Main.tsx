import { NavLink } from 'react-router-dom';

/** Компонент Главной страницы. */
export const Main = (): JSX.Element => (
    <div>
        <h1>Main Page</h1>
        <NavLink to="admin" >Admin</NavLink>
    </div>
);
