import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { EUserRole } from 'Consts/Consts';
import { IUserModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { requestAllRegionReports } from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода отчетов по регионам. */
export const GeneralReports = (): JSX.Element => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    /** Все отчеты. */
    const allReports = useSelector<TStore, any[]>(store => store.report.allRegionReports);
    /** Текущий пользователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);

    useEffect(() => {
        if (currentUser?.account?.role === EUserRole.GENERAL_MANAGER){
            dispatch(requestAllRegionReports());
        }
    }, [currentUser]);

    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/reports/region-report/${id}`);
    };

    /** Рендер последних отчетов. */
    const renderLastReports = (): JSX.Element => (
        <div className="start-shift__reports-container">
            <h3>Последние отчеты</h3>
            <TableContainer component={Paper}>
                <Table size="small" aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Регион</TableCell>
                            <TableCell className={classes.headerCell}>Дата</TableCell>
                            <TableCell className={classes.headerCell}>Выручка</TableCell>
                            <TableCell className={classes.headerCell}>Расходы</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {allReports?.map((r) => (
                            <TableRow
                                className="reports__table-row--href"
                                onClick={(): void => handleItemClick(r.id)}
                                key={`last-report-item-${r.id}`}
                            >
                                <TableCell component="th" scope="row">{r?.region.name ?? ''}</TableCell>
                                <TableCell component="th" scope="row">{r.date}</TableCell>
                                <TableCell component="th" scope="row">{r.total}</TableCell>
                                <TableCell component="th" scope="row">{r.expenses}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );

    return (
        <div>
            {renderLastReports()}
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
