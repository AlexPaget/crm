import {
    Button,
    Container,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import { ICityModel } from 'Models/Models';
import Paper from '@material-ui/core/Paper';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TStore } from 'Store/Store';
import { requestAllCities } from 'Store/Cities/CitiesReducer';
import { Spinner } from 'Components/LoadingOverlay/Spiner';
import { AddCityForm } from './AddCityForm';

/** Компонент вывода городов в админ панели. */
export const Cities = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список городов. */
    const citiesList = useSelector<TStore, ICityModel[]>(store => store.cities.citiesList);
    /** Индикатор лоадера. */
    const isLoading = useSelector<TStore, boolean>(store => store.cities.isLoading);

    /** Флаг отображения формы добавления города. */
    const [isAddFormVisible, setIsAddFormVisible] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestAllCities());
    }, []);

    /** Рендер элемента города.
     * 
     * @param item Элемент списка городов.
     */
    const renderCityItem = (item: ICityModel): JSX.Element => (
        <TableRow key={`city-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.name}</TableCell>
            <TableCell>{item?.manager?.userName}</TableCell>
            <TableCell>{item?.region?.name}</TableCell>
        </TableRow>
    );

    /** Рендер списка городов. */
    const renderCitiesList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Название</TableCell>
                        <TableCell className={classes.headerCell}>Управляющий</TableCell>
                        <TableCell className={classes.headerCell}>Регион</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {citiesList?.map((city) => renderCityItem(city))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    /** Рендер панели с контролами. */
    const renderControlPanel = (): JSX.Element => (
        <div className={classes.controlPanel}>
            <Button
                onClick={(): void => setIsAddFormVisible((p) => !p)}
                variant="contained"
                color={isAddFormVisible ? 'inherit' : 'primary'}
            >
                {`${isAddFormVisible ? 'скрыть' : 'Добавить город'}`}
            </Button>
            {isAddFormVisible && <AddCityForm />}
        </div>
    );

    return (
        <>
            <h1>Города</h1>
            <Container>
                {renderControlPanel()}
                {isLoading ? <Spinner /> : renderCitiesList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    },
}));
