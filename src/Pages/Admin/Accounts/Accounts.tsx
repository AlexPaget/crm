import {
    Button,
    Container,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Spinner } from 'Components/LoadingOverlay/Spiner';
import { IAccountModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { requestAllAccounts } from 'Store/Accounts/AccountsReducer';
import { TStore } from 'Store/Store';
import { roleNameConf } from 'Utils/Utils';
import { AddAccountForm } from './AddAccountForm';

/** Компонент вывода аккаунтов в админ панели. */
export const Accounts = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список аккаунтов. */
    const accountsList = useSelector<TStore, IAccountModel[]>(store => store.accounts.accountsList);
    /** Индикатор загрузки. */
    const isLoading = useSelector<TStore, boolean>(store => store.accounts.isLoading);

    /** Флаг отображения формы добавления аккаунта. */
    const [isAddFormVisible, setIsAddFormVisible] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestAllAccounts());
    }, []);

    /** Рендер элемента.
     * 
     * @param item Элемент списка аккаунтов.
     */
    const renderAccountItem = (item: IAccountModel): JSX.Element => (
        <TableRow key={`account-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.login}</TableCell>
            <TableCell>{roleNameConf[item.role]}</TableCell>
            <TableCell align="center">{renderStatus(item.isActive)}</TableCell>
        </TableRow>
    );

    /** Рендер статуса.
     *
     * @param isActive
     */
    const renderStatus = (isActive: boolean): JSX.Element =>
        <span className={isActive ? classes.statusActive : classes.statusNotActive}>
            {isActive ? <strong>&#10004;</strong> : <strong>&#10006;</strong>}
        </span>;

    /** Рендер списка аккаунтов. */
    const renderAccountsList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Логин</TableCell>
                        <TableCell className={classes.headerCell}>Должность</TableCell>
                        <TableCell className={classes.headerCell} align="center">Активен</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {accountsList?.map((account) => renderAccountItem(account))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    /** Рендер панели с контролами. */
    const renderControlPanel = (): JSX.Element => (
        <div className={classes.controlPanel}>
            <Button
                onClick={(): void => setIsAddFormVisible((p) => !p)}
                variant="contained"
                color={isAddFormVisible ? 'inherit' : 'primary'}
            >
                {`${isAddFormVisible ? 'скрыть' : 'Добавить аккаунт'}`}
            </Button>
            {isAddFormVisible && <AddAccountForm />}
        </div>
    );

    return (
        <>
            <h1>Аккаунты</h1>
            <Container>
                {renderControlPanel()}
                {isLoading ? <Spinner /> : renderAccountsList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    },
    statusActive: {
        color: 'green',
    },
    statusNotActive: {
        color: 'red',
    }
}));
