import { Button } from '@material-ui/core';
import { EUserRole } from 'Consts/Consts';
import { IUserModel } from 'Models/Models';
import { FC } from 'react';
import { useSelector } from 'react-redux';
import { Route, Switch, useRouteMatch, useHistory, Redirect } from 'react-router-dom';
import { TStore } from 'Store/Store';
import { CityReportCard } from './CityReportCard';
import { CityReports } from './CityReports';
import { CreateRegionReportForm } from './CreateRegionReportForm';
import { CreateCityReportForm } from './CreateReportForm';
import { GeneralReports } from './GeneralReports';
import { RegionReportCard } from './RegionReportCard';
import { RegionReports } from './RegionReports';
import './reports.scss';
import { ShiftReports } from './ShiftReports';

/** Компонент страницы работы с отчетами. */
export const ReportsContent: FC = (): JSX.Element => {
    const history = useHistory();
    const { path } = useRouteMatch();

    /** Текущий потльзователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);

    /** Рендер отчетов.
     * TODO завести отдельные компоненты
     */
    const renderReports = (): JSX.Element => (
        <div>
            {currentUser?.account?.role === EUserRole.CITY_MANAGER && (
                <>
                    <Button variant="contained" onClick={handleCreateLink}>Новый отчет</Button>
                    <ShiftReports />
                    <CityReports />
                </>
            )}
            {currentUser?.account?.role === EUserRole.REGION_MANAGER && (
                <>
                    <Button variant="contained" onClick={handleCreateLink}>Новый отчет</Button>
                    <RegionReports />
                </>
            )}
            {currentUser?.account?.role === EUserRole.GENERAL_MANAGER && (
                <GeneralReports />
            )}
        </div>
    );

    /** Получить компонент формы создания отчета. */
    const getReportForm = (): JSX.Element => {
        if (currentUser?.account?.role === EUserRole.CITY_MANAGER) {
            return <CreateCityReportForm />;
        }
        if (currentUser?.account?.role === EUserRole.REGION_MANAGER) {
            return <CreateRegionReportForm />;
        }

        return <div />;
    };

    /** Обработчик перехода на страницу создания отчета. */
    const handleCreateLink = (): void => history.push(`${path}/create`);

    return (
        <Switch>
            <Route exact path={path}>
                {renderReports()}
            </Route>
            <Route path={`${path}/create`}>
                {getReportForm()}
            </Route>
            <Route path={`${path}/region-report/:id`}>
                <RegionReportCard />
            </Route>
            <Route path={`${path}/city-report/:id`}>
                <CityReportCard />
            </Route>
            <Redirect to={path} />
        </Switch>
    );
};
