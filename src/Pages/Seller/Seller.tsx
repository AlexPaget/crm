import { IMarketModel, IUserModel } from 'Models/Models';
import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { AuthService } from 'Services/AuthService';
import { requestActiveShift } from 'Store/Shift/ShiftReducer';
import { TStore } from 'Store/Store';
import { requestUserInfo } from 'Store/Users/UsersReducer';
import './seller.scss';
import { StartShiftMenu } from './StartShiftMenu/StartShiftMenu';
import { WorkShift } from './WorkShift/WorkShift';

/** Компонент страницы продавца. */
export const Seller = (): JSX.Element => {
    const history = useHistory();
    const dispatch = useDispatch();

    /** Модель точки. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);
    /** Идентификатор активной смены. */
    const activeShift = useSelector<TStore, number | null>(store => store.shift.activeShift);
    /** Идентификатор активной смены. */
    const currentMarket =
    useSelector<TStore, IMarketModel | null>(store => store.shift.currentMarketShift);

    useEffect(() => {
        dispatch(requestUserInfo());
    }, []);

    useEffect(() => {
        currentUser && dispatch(requestActiveShift(currentUser.id));
    }, [currentUser]);

    /** Обработчик кнопки выхода из аккаунта. */
    const handleLogoutClick = useCallback((): void => {
        AuthService.logout(() => history.push('/auth'));
    }, []);

    /** Рендер названия рабочей точки. */
    const renderPointName = (): JSX.Element => (
        <div className="seller__header">
            {renderCurrentUserBlock()}
            {currentMarket && <h3>{currentMarket.name}</h3>}
            <button onClick={handleLogoutClick}>Выход</button>
        </div>
    );

    /** Рендер блока текущего пользователя. */
    const renderCurrentUserBlock = (): JSX.Element => (
        <div className="seller__current-user">
            {currentUser && <span>{currentUser.userName}</span>}
        </div>
    );

    /** Рендер сайд бар. */
    const renderSideBar = (): JSX.Element => (
        <div className="seller__side-bar">
            <li>Касса</li>
            <li>Склад</li>
        </div>
    );

    return (
        <div className="seller">
            {renderSideBar()}
            <div className="seller__content">
                {renderPointName()}
                {activeShift ? <WorkShift /> : <StartShiftMenu />}
            </div>
        </div>
    );
};
