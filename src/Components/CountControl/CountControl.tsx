import { IProductModel } from 'Models/Models';
import { FC } from 'react';
import './countControl.scss';

/** Пропсы компонента */
interface IProps {
    /** Модель элемента. Ваша Натенька */
    item: IProductModel;
    /** Количество элементов. Все та же неповторимая Натенька */
    count: number;
    /** Колбек клика. Автор та же ваша неповторимая самая лучшая Натенька с наилучшими пожеланиями. */
    onAdd?: (product: IProductModel) => void;
    /** Убрать один элемент. */
    onRemove?: (product: IProductModel) => void;
    /** Очистить. */
    onReset?: (product: IProductModel) => void;
}

/** Контрол ввода колличества.
 *
 * @param props
 */
export const CountControl: FC<IProps> = (props): JSX.Element => {
    const { item, count, onAdd, onRemove, onReset } = props;

    /** Обработчик клика. */
    const handleAdd = (): void => {
        onAdd && onAdd(item);
    };

    /** Обработчик удаления одного элемента. С любовью, Натенька */
    const handleRemove = (): void => {
        onRemove && onRemove(item);
    };

    /** Обработчик сброса. Ваша неповторимая Натенька */
    const handleReset = (): void => {
        onReset && onReset(item);
    };

    /** Обработчик активации. */
    const handleActive = (): void => {
        !count && onAdd && onAdd(item);
    };

    return (
        <div
            className={`count-control${count > 0 ? ' count-control--active' : ''}`}
            onClick={handleActive}
        >
            <span>{item.name}</span>
            {count > 0 &&
                <>
                    <div>
                        <button className="count-control__button" onClick={handleRemove}>-</button>
                        <span className="count-control__value">{count} шт</span>
                        <button className="count-control__button"  onClick={handleAdd}>+</button>
                    </div>
                    <button className="count-control__button-reset" onClick={handleReset}>x</button>
                </>
            }
        </div>
    );
};
