import {
    Button,
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { reportAction, requestCityReportById} from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода подробной информации о отчете. */
export const CityReportCard = (): JSX.Element => {

    /** Модель регионального отчета. */
    const report = useSelector<TStore, any | null>(store => store.report.cityReportCard);

    const classes = useStyles();
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(requestCityReportById(id));

        return (): void => {
            dispatch(reportAction.clearCityReportCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };
    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/reports/shift-report/${id}`);
    };

    /** Элемент отчета города.
     *
     * @param el
     */
    const renderShiftReportItem = (el: any): JSX.Element => (
        <TableRow
            key={`shift-report-${el.id}`}
            className="reports__table-row--href"
            onClick={(): void => handleItemClick(el.id)}
        >
            <TableCell />
            <TableCell component="th" scope="row">{el?.shift.market.name}</TableCell>
            <TableCell component="th" scope="row">{el.transfer}</TableCell>
            <TableCell component="th" scope="row">{el.cash}</TableCell>
            <TableCell component="th" scope="row">{el.cashless}</TableCell>
            <TableCell component="th" scope="row">{el.total}</TableCell>
            <TableCell component="th" scope="row">{el.expenses}</TableCell>
        </TableRow>
    );

    /** Рендер общей информации. */
    const renderMainInfo = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table size="small" aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Дата</TableCell>
                        <TableCell className={classes.headerCell}>Город</TableCell>
                        <TableCell className={classes.headerCell}>Перевод</TableCell>
                        <TableCell className={classes.headerCell}>Наличные</TableCell>
                        <TableCell className={classes.headerCell}>Безнал</TableCell>
                        <TableCell className={classes.headerCell}>Выручка</TableCell>
                        <TableCell className={classes.headerCell}>Расходы</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell component="th" scope="row">{report.date}</TableCell>
                        <TableCell component="th" scope="row">{report?.city.name ?? ''}</TableCell>
                        <TableCell component="th" scope="row">{report.transfer}</TableCell>
                        <TableCell component="th" scope="row">{report.cash}</TableCell>
                        <TableCell component="th" scope="row">{report.cashless}</TableCell>
                        <TableCell component="th" scope="row">{report.total}</TableCell>
                        <TableCell component="th" scope="row">{report.expenses}</TableCell>
                    </TableRow>
                </TableBody>
            </Table>
            <br />
            <TableRow>Включает в себя:</TableRow>
            <Table size="small" aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell />
                        <TableCell className={classes.headerCell}>Город</TableCell>
                        <TableCell className={classes.headerCell}>Перевод</TableCell>
                        <TableCell className={classes.headerCell}>Наличные</TableCell>
                        <TableCell className={classes.headerCell}>Безнал</TableCell>
                        <TableCell className={classes.headerCell}>Выручка</TableCell>
                        <TableCell className={classes.headerCell}>Расходы</TableCell>
                    </TableRow>
                </TableHead>
                {report && report.shiftReports.map((rep: any) => (
                    <TableBody key={`city-reports-${rep.id}`}>
                        {renderShiftReportItem(rep)}
                    </TableBody>
                ))}
            </Table>
        </TableContainer>
    );

    return (
        <div className="reports__card">
            <Button
                variant="contained"
                size="small"
                onClick={handleBackButtonPress}
            >
                назад
            </Button>
            {!!report && renderMainInfo()}
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '16px',
        fontWeight: theme.typography.fontWeightBold,
    }
}));
