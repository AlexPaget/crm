import { makeStyles, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import { useEffect } from 'react';
import { IUserModel } from 'Models/Models';
import { useDispatch, useSelector } from 'react-redux';
import { requestRawShifts } from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода отчетов о сменах. */
export const ShiftReports = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();
    /** Отчеты. */
    const reports = useSelector<TStore, any[]>(store => store.report.reportsList);
    /** Текущий потльзователь. */
    const currentUser = useSelector<TStore, IUserModel | null>(store => store.users.currentUser);

    useEffect(() => {
        if (currentUser?.managedCityId){
            dispatch(requestRawShifts(currentUser.managedCityId));
        }
    }, []);

    return (
        <div className="start-shift__reports-container">
            <h3>Смены без отчета</h3>
            <TableContainer component={Paper}>
                <Table size='small' aria-label="customized table">
                    <TableHead className={classes.tableHead}>
                        <TableRow>
                            <TableCell className={classes.headerCell}>Дата</TableCell>
                            <TableCell className={classes.headerCell}>Точка</TableCell>
                            <TableCell className={classes.headerCell}>Продавец</TableCell>
                            <TableCell className={classes.headerCell}>Выручка</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {reports && reports?.map((r) => (
                            <TableRow key={`report-item-${r.id}`}>
                                <TableCell component="th" scope="row">{r.shift.date}</TableCell>
                                <TableCell align="center" component="th" scope="row">{r.shift.market.name}</TableCell>
                                <TableCell
                                    align="center"
                                    component="th"
                                    scope="row"
                                >{r.shift.seller.userName}</TableCell>
                                <TableCell align="center" component="th" scope="row">{r.total}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
