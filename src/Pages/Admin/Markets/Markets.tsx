import {
    Button,
    Container,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { Spinner } from 'Components/LoadingOverlay/Spiner';
import { IMarketModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { requestAllMarkets } from 'Store/Markets/MarketsReducer';
import { TStore } from 'Store/Store';
import { AddMarketForm } from './AddMarketForm';

/** Компонент вывода торговых точек в админ панели. */
export const Markets = (): JSX.Element => {
    const classes = useStyles();
    const dispatch = useDispatch();

    /** Список торговых точек. */
    const marketsList = useSelector<TStore, IMarketModel[]>(store => store.markets.marketsList);
    /** Индикатор загрузки торговых точек. */
    const isLoading = useSelector<TStore, boolean>(store => store.markets.isLoading);

    /** Флаг отображения формы добавления торговой точки. */
    const [isAddFormVisible, setIsAddFormVisible] = useState<boolean>(false);

    useEffect(() => {
        dispatch(requestAllMarkets());
    }, []);

    /** Рендер элемента торговой точки.
     * 
     * @param item Элемент списка торговых точек.
     */
    const renderMarketItem = (item: IMarketModel): JSX.Element => (
        <TableRow key={`region-item-${item.id}`}>
            <TableCell component="th" scope="row">{item.name}</TableCell>
            <TableCell>{item.city.name}</TableCell>
            <TableCell>{item.adress}</TableCell>
        </TableRow>
    );

    /** Рендер списка торговых точек. */
    const renderMarketList = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Название</TableCell>
                        <TableCell className={classes.headerCell}>Город</TableCell>
                        <TableCell className={classes.headerCell}>Адрес</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {marketsList?.map((market) => renderMarketItem(market))}
                </TableBody>
            </Table>
        </TableContainer>
    );

    /** Рендер панели с контролами. */
    const renderControlPanel = (): JSX.Element => (
        <div className={classes.controlPanel}>
            <Button
                onClick={(): void => setIsAddFormVisible((p) => !p)}
                variant="contained"
                color={isAddFormVisible ? 'inherit' : 'primary'}
            >
                {`${isAddFormVisible ? 'скрыть' : 'Добавить точку'}`}
            </Button>
            {isAddFormVisible && <AddMarketForm />}
        </div>
    );

    return (
        <>
            <h1>Торговые точки</h1>
            <Container>
                {renderControlPanel()}
                {isLoading ? <Spinner /> : renderMarketList()}
            </Container>
        </>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 700,
    },
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '18px',
        fontWeight: theme.typography.fontWeightBold,
    },
    controlPanel: {
        padding: theme.spacing(1, 0),
    }
}));
