import { Button } from '@material-ui/core';
import { IMarketModel } from 'Models/Models';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory, NavLink } from 'react-router-dom';
import { marketsAction, requestMarketById } from 'Store/Markets/MarketsReducer';
import { TStore } from 'Store/Store';
import { ProductsBlock } from '../ProductsBlock/ProductsBlock';
import { SellersBlock } from '../SellersBlock/SellersBlock';
import './pointCard.scss';

/** Карточка рабочей точки.
 *
 * @param props
 */
export const PointCard = (): JSX.Element => {
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Модель точки. */
    const marketCard = useSelector<TStore, IMarketModel | null>(store => store.markets.marketCard);

    useEffect(() => {
        dispatch(requestMarketById(id));

        return (): void => {
            dispatch(marketsAction.clearMarketCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };

    /** Рендер менеджера. */
    const renderManagerData = (): JSX.Element => (
        <div>
            <span>Управляющий: </span>
            {marketCard?.city?.manager?.userName
                ? <NavLink
                    className="navlink--text"
                    to={`/users/${marketCard.city.manager.id}`}
                >
                    {marketCard.city.manager.userName}
                </NavLink>
                : <span>нет</span>}
        </div>
    );

    return (
        <div>
            <Button
                variant="contained"
                size="small"
                onClick={handleBackButtonPress}
            >
                назад
            </Button>
            {marketCard && <h2>{marketCard?.name}</h2>}
            {renderManagerData()}
            <div className="point-card__blocks-container">
                <SellersBlock sellers={marketCard?.sellers} />
                <ProductsBlock products={marketCard?.products} />
            </div>
        </div>
    );
};
