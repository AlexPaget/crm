import {
    Button,
    makeStyles,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { reportAction, requestRegionReportById } from 'Store/Reports/ReportsReducer';
import { TStore } from 'Store/Store';

/** Компонент вывода подробной информации о отчете. */
export const RegionReportCard = (): JSX.Element => {
    const classes = useStyles();
    const { id } = useParams<any>();
    const history = useHistory();
    const dispatch = useDispatch();

    /** Модель регионального отчета. */
    const report = useSelector<TStore, any | null>(store => store.report.regionReportCard);

    useEffect(() => {
        dispatch(requestRegionReportById(id));

        return (): void => {
            dispatch(reportAction.clearRegionReportCard());
        };
    }, []);

    /** Обработчик кнопки "назад". */
    const handleBackButtonPress = (): void => {
        history.goBack();
    };
    /** Обработчик клика на элемент.
     *
     * @param id
     */
    const handleItemClick = (id: number): void => {
        history.push(`/reports/city-report/${id}`);
    };

    /** Элемент отчета города.
     *
     * @param item
     */
    const renderCityReportItem = (item: any): JSX.Element => (
        <TableRow
            key={`city-report-${item.id}`}
            className="reports__table-row--href"
            onClick={(): void => handleItemClick(item.id)}
        >
            <TableCell />
            <TableCell component="th" scope="row">{item?.city.name ?? ''}</TableCell>
            <TableCell component="th" scope="row">{item.transfer}</TableCell>
            <TableCell component="th" scope="row">{item.cash}</TableCell>
            <TableCell component="th" scope="row">{item.cashless}</TableCell>
            <TableCell component="th" scope="row">{item.total}</TableCell>
            <TableCell component="th" scope="row">{item.expenses}</TableCell>
        </TableRow>
    );

    /** Рендер общей информации. */
    const renderMainInfo = (): JSX.Element => (
        <TableContainer component={Paper}>
            <Table size="small" aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell className={classes.headerCell}>Дата</TableCell>
                        <TableCell className={classes.headerCell}>Регион</TableCell>
                        <TableCell className={classes.headerCell}>Перевод</TableCell>
                        <TableCell className={classes.headerCell}>Наличные</TableCell>
                        <TableCell className={classes.headerCell}>Безнал</TableCell>
                        <TableCell className={classes.headerCell}>Выручка</TableCell>
                        <TableCell className={classes.headerCell}>Расходы</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell component="th" scope="row">{report.date}</TableCell>
                        <TableCell component="th" scope="row">{report?.region.name ?? ''}</TableCell>
                        <TableCell component="th" scope="row">{report.transfer}</TableCell>
                        <TableCell component="th" scope="row">{report.cash}</TableCell>
                        <TableCell component="th" scope="row">{report.cashless}</TableCell>
                        <TableCell component="th" scope="row">{report.total}</TableCell>
                        <TableCell component="th" scope="row">{report.expenses}</TableCell>
                    </TableRow>
                </TableBody>

            </Table>
            <br />
            <TableRow>Включает в себя:</TableRow>
            <Table size="small" aria-label="customized table">
                <TableHead className={classes.tableHead}>
                    <TableRow>
                        <TableCell />
                        <TableCell className={classes.headerCell}>Город</TableCell>
                        <TableCell className={classes.headerCell}>Перевод</TableCell>
                        <TableCell className={classes.headerCell}>Наличные</TableCell>
                        <TableCell className={classes.headerCell}>Безнал</TableCell>
                        <TableCell className={classes.headerCell}>Выручка</TableCell>
                        <TableCell className={classes.headerCell}>Расходы</TableCell>
                    </TableRow>
                </TableHead>
                {report.cityReports.map((item: any) => (
                    <TableBody key={`city-reports-${item.id}`}>
                        {renderCityReportItem(item)}
                    </TableBody>
                ))}
            </Table>
        </TableContainer>
    );

    return (
        <div className="reports__card">
            <Button
                variant="contained"
                size="small"
                onClick={handleBackButtonPress}
            >
                назад
            </Button>
            {report && renderMainInfo()}
        </div>
    );
};

/** Стили компонента. */
const useStyles = makeStyles((theme) => ({
    tableHead: {
        backgroundColor: theme.palette.primary.light,
    },
    headerCell: {
        color: theme.palette.background.default,
        fontSize: '16px',
        fontWeight: theme.typography.fontWeightBold,
    }
}));
