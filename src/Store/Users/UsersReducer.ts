import { API } from 'Api/Api';
import { IUserModel } from 'Models/Models';
import { EUserRole } from 'Consts/Consts';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - UsersReducer. */
export enum EUsersAction {
    /** Добавить инфо текущего пользователя. */
    SET_CURRENT_USER = 'users/SET_CURRENT_USER',
    /** Добавить инфо о пользователе для карточки. */
    SET_USER_CARD = 'users/SET_USER_CARD',
    /** Очистить карточку пользователя. */
    CLEAR_USER_CARD = 'users/CLEAR_USER_CARD',
    /** Добавить пользователей. */
    ADD_USERS_LIST = 'users/ADD_USERS_LIST',
    /** Переключение индикатора загрузки пользователей. */
    TOGGLE_LOADING = 'users/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Текущий пользователь. */
    currentUser: IUserModel | null;
    /** Информация о пользователе в карточке. */
    userCard: IUserModel | null;
    /** Список пользователей. */
    usersList: IUserModel[];
    /** Индикатор загрузки пользователей. */
    isLoading: boolean;
}

const initialState: IState = {
    currentUser: null,
    userCard: null,
    usersList: [],
    isLoading: false
};

/** Редюсер пользователей.
 * 
 * @param state
 * @param action 
 */
export const usersReducer = (state = initialState, action: TReducerAction<typeof usersAction>): IState => {
    switch (action.type) {
        case (EUsersAction.SET_CURRENT_USER):
            return { ...state, currentUser: action.userInfo };
        case (EUsersAction.SET_USER_CARD):
            return { ...state, userCard: action.userCard };
        case (EUsersAction.CLEAR_USER_CARD):
            return { ...state, userCard: null };
        case (EUsersAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.isLoading };
        case (EUsersAction.ADD_USERS_LIST):
            return {
                ...state,
                usersList: action.usersList
            };
        default:
            return state;
    }
};

export const usersAction = {
    /** Добавить инфо о текущем пользователе.
     *
     * @param userInfo
     */
    setCurrentUser: (userInfo: IUserModel) => (
        { type: EUsersAction.SET_CURRENT_USER, userInfo } as const
    ),
    /** Добавить инфрормацию для карточки пользователя.
     * 
     * @param userCard
     */
    setUserCard: (userCard: IUserModel) => (
        { type: EUsersAction.SET_USER_CARD, userCard } as const
    ),
    /** Очистить карточку пользователя. */
    clearUserCard: () => ({ type: EUsersAction.CLEAR_USER_CARD } as const),
    /** Добавить список пользователей.
     * 
     * @param usersList Список пользователей.
     */
    addUsersList: (usersList: IUserModel[]) => (
        { type: EUsersAction.ADD_USERS_LIST, usersList } as const
    ),
    /** Переключить индикатор загрузки пользователей.
     * 
     * @param isLoading Значение индикатора.
     */
    toggleLoading: (isLoading: boolean) => (
        { type: EUsersAction.TOGGLE_LOADING, isLoading } as const
    )
};

/** Получить пользователей по роли.
 * 
 * @param role
 */
export const requestUsersByRole = (role: EUserRole): ThunkType => (dispatch): any => {
    dispatch(usersAction.toggleLoading(true));
    API.GET(`api/users/role/${role}`)
        .then((res: any) => {
            dispatch(usersAction.addUsersList(res.data));
            dispatch(usersAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(usersAction.toggleLoading(false));
        });
};

/** Получить список пользователей. */
export const requestAllUsers = (): ThunkType => (dispatch): any => {
    dispatch(usersAction.toggleLoading(true));
    API.GET('api/users')
        .then((res: any) => {
            dispatch(usersAction.addUsersList(res.data));
            dispatch(usersAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(usersAction.toggleLoading(false));
        });
};

/** Получить данные о текущем пользователе. */
export const requestUserInfo = (): ThunkType => (dispatch): any => {
    API.GET('api/users/info')
        .then((res: any) => dispatch(usersAction.setCurrentUser(res.data)));
};

/** Получить пользователя по ID.
 * 
 * @param id
 */
export const requestUserById = (id: number): ThunkType => (dispatch): any => {
    dispatch(usersAction.toggleLoading(true));
    API.GET(`api/users/card/${id}`)
        .then((res: any) => {
            dispatch(usersAction.setUserCard(res.data));
            dispatch(usersAction.toggleLoading(false));
        })
        .catch(() => dispatch(usersAction.toggleLoading(false)));
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof usersAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
