import {
    ListItemIcon,
    ListItemText,
    MenuItem,
    MenuList,
    Paper,
    Popover,
    TableCell,
    TableRow
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/MoreVert';
import DeleteIcon from '@material-ui/icons/Delete';
import './sellersBlock.scss';
import { useState } from 'react';
import { NavLink } from 'react-router-dom';

/** Модель свойтв кмпонента. */
interface IProps {
    /** Id */
    id: number;
    /** Имя. */
    name: string;
    /** Коллбек удаления товара. */
    onRemove?: (id: number) => void;
}

/** Компонент продавца.
 * 
 * @param props
 */
export const SellerItem = (props: IProps): JSX.Element => {
    const { id, name, onRemove } = props;

    const [anchorEl, setAnchorEl] = useState(null);

    const open = Boolean(anchorEl);
    const popoverId = open ? 'product-item' : undefined;

    /** Обработчик открытия поповера.
     * 
     * @param event
     */
    const handlePopoverClick = (event: any): void => {
        event?.currentTarget && setAnchorEl(event.currentTarget);
    };

    /** Обработчик закрытия поповера. */
    const handlePopoverClose = (): void => {
        setAnchorEl(null);
    };

    /** Обработчик удаления товара из точки. */
    const handleClickRemove = (): void => {
        onRemove && onRemove(id);
        setAnchorEl(null);
    };

    /** Рендер меню. */
    const renderItemMenu = (): JSX.Element => (
        <Paper>
            <MenuList>
                <MenuItem onClick={handleClickRemove}>
                    <ListItemIcon>
                        <DeleteIcon fontSize="small" />
                    </ListItemIcon>
                    <ListItemText>Убрать</ListItemText>
                </MenuItem>
            </MenuList>
        </Paper>
    );

    return (
        <TableRow className="sellers-block__item">
            <TableCell>
                <NavLink className="navlink navlink--bold" to={`/users/${id}`}>{name}</NavLink>
            </TableCell>
            <TableCell
                align="center"
                className="products-block__item-price"
            >
                <MenuIcon
                    fontSize="small"
                    onClick={handlePopoverClick}
                    className="products-block__menu-button"
                />
                <Popover
                    id={popoverId}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handlePopoverClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                >
                    {renderItemMenu()}
                </Popover>
            </TableCell>
        </TableRow>
    );
};
