import { Button, Dialog, DialogTitle } from '@material-ui/core';
import { CountControl } from 'Components/CountControl/CountControl';
import { IMarketModel, IProductModel } from 'Models/Models';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { requestMarketShift, requestShiftOrders, sendOrder } from 'Store/Shift/ShiftReducer';
import { TStore } from 'Store/Store';
import { ShiftCloseForm } from './ShiftCloseForm';
import './workShift.scss';

/** Модель чека. */
interface IOrderParam {
    /** Количество. */
    count: number;
}

/** Тип оплаты. */
export enum EPayType {
    /** Не установлена. */
    NONE = 'NONE',
    /** Безнал */
    CASHLESS = 'CASHLESS',
    /** Наличные. */
    CASH = 'CASH',
    /** Перевод. */
    TRANSFER = 'TRANSFER'
}

/** Конфиг перевода */
const payTypeConf: any = {
    [EPayType.NONE]: 'Не выбран',
    [EPayType.CASHLESS]: 'Безнал',
    [EPayType.CASH]: 'Наличные',
    [EPayType.TRANSFER]: 'Перевод'
};

/** Компонент рабочей смены. */
export const WorkShift = (): JSX.Element => {
    const dispatch = useDispatch();

    /** Активная смена. */
    const activeShift =
        useSelector<TStore, any>(store => store.shift.activeShift);
    /** Идентификатор активной смены. */
    const currentMarket =
        useSelector<TStore, IMarketModel | null>(store => store.shift.currentMarketShift);
    /** Список операций. */
    const ordersList = useSelector<TStore, any[]>(store => store.shift.ordersList);

    /** Мапа товаров в чеке. */
    const [order, setOrder] = useState<Map<IProductModel, IOrderParam>>(new Map());
    const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
    const [isShiftDialogOpen, setIsShiftDialogOpen] = useState<boolean>(false);
    const [activePayType, setActivePayType] = useState<EPayType>(EPayType.NONE);
    const [isPayTypeError, setIsPayTypeError] = useState<boolean>(false);

    useEffect(() => {
        activeShift.marketId && dispatch(requestMarketShift(activeShift.marketId));
        dispatch(requestShiftOrders(activeShift.id));
    }, []);

    /** Готово */
    const handleReadyClick = (): void => {
        setIsDialogOpen(true);
    };

    /** Обработчик закрытия окна. */
    const handleCloseDialog = (): void => {
        setIsDialogOpen(false);
    };

    /** Обработчик закрытия окна смены. */
    const handleCloseShiftDialog = (): void => {
        setIsShiftDialogOpen(false);
    };

    /** Рендер списка элементов чека. */
    const renderOrderItems = (): JSX.Element[] => {
        const result: JSX.Element[] = [];

        order.forEach((value, key) => {
            result.push(
                <div key={`order-item-${key.id}`}>
                    <span><strong>{key.name}</strong></span>
                    <br />
                    <span>К-во: {value.count} шт</span>
                    <br />
                    <span>Цена: {key.ProductMarket.price} р</span>
                    <br />
                    <span>Всего: {value.count * key.ProductMarket.price} р</span>
                    <br />
                    <br />
                </div>
            );
        });

        return result;
    };

    /** Рендер итогового счета. */
    const renderTotal = (): JSX.Element => {
        let totalCount = 0;
        order.forEach((value, key) => {
            totalCount += value.count * key.ProductMarket.price;
        });

        return (
            <div>
                <hr />
                <h3>Итого: {totalCount} р</h3>
                <Button variant="contained" onClick={handleReadyClick}>
                    Готово
                </Button>
            </div>
        );
    };

    /** Отправить чек. */
    const handleSendOrder = (): void => {
        let totalPrice = 0;
        order.forEach((value, key) => {
            totalPrice += value.count * key.ProductMarket.price;
        });
        const orderProducts: any[] = [];
        order.forEach((value, key) => {
            orderProducts.push({
                productId: key.id,
                count: value.count,
                price: key.ProductMarket.price,
            });
        });
        const data: any = {
            shiftId: activeShift.id,
            totalPrice: totalPrice,
            payType: activePayType,
            products: orderProducts,
        };
        dispatch(sendOrder(data));
    };

    /** Обработчик подтверждения чека. */
    const handleConfirmOrder = (): void => {
        if (activePayType === EPayType.NONE) {
            setIsPayTypeError(true);

            return;
        }
        handleSendOrder();
        setIsDialogOpen(false);
        setOrder(new Map());
        setActivePayType(EPayType.NONE);
    };

    /** Рендер диалогового окна. */
    const renderDialog = (): JSX.Element => {
        let totalCount = 0;
        order.forEach((value, key) => {
            totalCount += value.count * key.ProductMarket.price;
        });

        return (
            <Dialog className="work-shift__dialog" onClose={handleCloseDialog} open={isDialogOpen}>
                <DialogTitle>{`Принято ${totalCount} р`}</DialogTitle>
                <p className={isPayTypeError ? 'work-shift__error-text' : ''}>Выберите тип оплаты</p>
                <div className="work-shift__pay">
                    <div
                        className={`work-shift__pay-type${activePayType === EPayType.CASHLESS
                            ? ' work-shift__pay-type--active' : ''}`}
                        onClick={(): void => {
                            setActivePayType(EPayType.CASHLESS);
                            setIsPayTypeError(false);
                        }}
                    ><span>Безнал</span></div>
                    <div
                        className={`work-shift__pay-type${activePayType === EPayType.CASH
                            ? ' work-shift__pay-type--active' : ''}`}
                        onClick={(): void => {
                            setActivePayType(EPayType.CASH);
                            setIsPayTypeError(false);
                        }}
                    ><span>Наличные</span></div>
                    <div
                        className={`work-shift__pay-type${activePayType === EPayType.TRANSFER
                            ? ' work-shift__pay-type--active' : ''}`}
                        onClick={(): void => {
                            setActivePayType(EPayType.TRANSFER);
                            setIsPayTypeError(false);
                        }}
                    ><span>Перевод</span></div>
                </div>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={handleConfirmOrder}
                >Подтвердить</Button>
            </Dialog>
        );
    };

    /** Рендер блока закрытия смены. */
    const renderCloseShiftBlock = (): JSX.Element => (
        <div>
            <Button
                variant="contained"
                onClick={(): void => setIsShiftDialogOpen(true)}
            >
                Закрыть смену
            </Button>
            <Dialog
                className="work-shift__dialog"
                onClose={handleCloseShiftDialog}
                open={isShiftDialogOpen}
            >
                <ShiftCloseForm onClose={handleCloseShiftDialog} />
            </Dialog>
        </div>
    );

    /** Рендер итога. */
    const renderOrder = (): JSX.Element => (
        <div className="seller__order">
            {renderOrderItems()}
            {order.size > 0 && renderTotal()}
            {renderDialog()}
            {renderCloseShiftBlock()}
        </div>
    );

    /** Добавить товар.
     * 
     * @param item
     */
    const handleAddProduct = (item: IProductModel): void => {
        setOrder((prev) => {
            if (prev.has(item)) {
                const prevCount = prev.get(item)?.count ?? 1;

                return new Map([...prev, [item, { count: prevCount + 1 }]]);
            }

            return new Map([...prev, [item, { count: 1 }]]);
        });
    };

    /** Убрать товар товар.
     * 
     * @param item
     */
    const handleRemoveProduct = (item: IProductModel): void => {
        setOrder((prev) => {
            if (prev.has(item)) {
                const prevCount = prev.get(item)?.count ?? 1;

                return new Map([...prev, [item, { count: prevCount - 1 }]]);
            }

            return new Map([...prev, [item, { count: 1 }]]);
        });
    };

    /** Сбросить товар.
     * 
     * @param item
     */
    const handleResetProduct = (item: IProductModel): void => {
        setOrder((prev) => {
            const newOrder = prev;
            newOrder.delete(item);

            return new Map([...newOrder]);
        });
    };

    /** Рендер товаров. */
    const renderProducts = (): JSX.Element => (
        <div className="seller__table-form">
            {currentMarket?.products.map((p) => (
                <CountControl
                    key={`product-${p.id}`}
                    count={order.get(p)?.count ?? 0}
                    item={p}
                    onAdd={handleAddProduct}
                    onRemove={handleRemoveProduct}
                    onReset={handleResetProduct}
                />
            ))}
        </div>
    );

    /** Рендер истории операций. */
    const renderHistory = (): JSX.Element => (
        <div className="seller__orders-list">
            <h3>История операций: </h3>
            {ordersList.map((o) => (
                <li
                    key={`order-${o.id}`}
                    className="seller__orders-list-item"
                >
                    <strong><span>#{o.id} </span></strong>
                    <span>
                        <span> {payTypeConf[o.payType] ?? ''} </span>
                        <span> {o.totalPrice} р</span>
                    </span>
                </li>
            ))}
        </div>
    );

    return (
        <div className="seller">
            {renderOrder()}
            <div className="seller__content">
                {renderProducts()}
                {renderHistory()}
            </div>
        </div>
    );
};
