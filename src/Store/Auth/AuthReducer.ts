import { IAuthData } from 'Models/Models';
import { TReducerAction } from 'Store/Store';

/** Actions - AuthReducer. */
export enum EAuthAction {
    /** Добавить данные авторизации. */
    SET_AUTH = 'auth/SET_AUTH',
    /** Удалить данные авторизации. */
    CREAR_AUTH = 'auth/CLEAR_AUTH',
    /** Переключить спиннер загрузки. */
    TOGGLE_LOADING = 'auth/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Данные авторизации. */
    data: IAuthData | null;
    /** Индикатор загрузки данных. */
    isLoading: boolean;
}

const initialState: IState = {
    data: null,
    isLoading: false
};

/** Редюсер авторизации.
 * 
 * @param state
 * @param action 
 */
export const authReducer = (state = initialState, action: TReducerAction<typeof authAction>): IState => {
    switch (action.type) {
        case (EAuthAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.value };
        case (EAuthAction.SET_AUTH):
            return {
                ...state,
                data: action.data
            };
        case (EAuthAction.CREAR_AUTH):
            return {
                ...state,
                data: null
            };
        default:
            return state;
    }
};

export const authAction = {
    /** Добавить данные авторизации.
     * 
     * @param data
     */
    setAuthData: (data: IAuthData | null) => ({ type: EAuthAction.SET_AUTH, data} as const),
    /** Очистить даные авторизации. */
    clearAuthData: () => ({ type: EAuthAction.CREAR_AUTH } as const),
    /** Переключить индикатор загрузки.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EAuthAction.TOGGLE_LOADING, value } as const
    )
};
