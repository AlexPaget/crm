import { API } from 'Api/Api';
import { IProductModel } from 'Models/Models';
import { BaseThunkType, TReducerAction } from 'Store/Store';

/** Actions - ProductsReducer. */
export enum EProductsAction {
    /** Добавить товар. */
    ADD_PRODUCT = 'products/ADD_PRODUCT',
    /** Добавить карточку продукта. */
    SET_PRODUCT_CARD = 'products/SET_PRODUCT_CARD',
    /** Очистить карточку. */
    CLEAR_PRODUCT_CARD = 'products/CLEAR_PRODUCT_CARD',
    /** Добавить список товаров. */
    SET_PRODUCTS_LIST = 'products/SET_PRODUCTS_LIST',
    /** Добавить доступные товары. */
    SET_AVAILABLE_PRODUCTS = 'products/SET_AVAILABLE_PRODUCTS',
    /** Переключение индикатора загрузки товаров. */
    TOGGLE_LOADING = 'products/TOGGLE_LOADING'
}

/** Модель состояния. */
interface IState {
    /** Список товаров. */
    productsList: IProductModel[];
    /** Карточка товара. */
    productCard: IProductModel | null;
    /** Доступные товары. */
    availableProducts: IProductModel[];
    /** Индикатор загрузки товаров. */
    isLoading: boolean;
}

const initialState: IState = {
    productsList: [],
    productCard: null,
    availableProducts: [],
    isLoading: false
};

/** Редюсер товаров.
 * 
 * @param state
 * @param action 
 */
export const productReducer = (state = initialState, action: TReducerAction<typeof productsAction>): IState => {
    switch (action.type) {
        case (EProductsAction.TOGGLE_LOADING):
            return { ...state, isLoading: action.value };
        case (EProductsAction.SET_PRODUCT_CARD):
            return { ...state, productCard: action.productCard };
        case (EProductsAction.CLEAR_PRODUCT_CARD):
            return { ...state, productCard: null };
        case (EProductsAction.ADD_PRODUCT):
            return {
                ...state,
                productsList: [
                    ...state.productsList,
                    action.product
                ]
            };
        case (EProductsAction.SET_PRODUCTS_LIST):
            return {
                ...state,
                productsList: action.productsList
            };
        case (EProductsAction.SET_AVAILABLE_PRODUCTS):
            return {
                ...state,
                availableProducts: action.availableProducts
            };
        default:
            return state;
    }
};

export const productsAction = {
    /** Добавить товар.
     * 
     * @param product Товар.
     */
    addProduct: (product: IProductModel) => ({ type: EProductsAction.ADD_PRODUCT, product } as const),
    /** Добавить карточку товара.
     * 
     * @param productCard
     */
    setProductCard: (productCard: IProductModel) => (
        { type: EProductsAction.SET_PRODUCT_CARD, productCard } as const
    ),
    /** Очистить карточку товара. */
    clearProductCard: () => ({ type: EProductsAction.CLEAR_PRODUCT_CARD } as const),
    /** Добавить список товаров.
     * 
     * @param productsList
     */
    addProductsList: (productsList: IProductModel[]) => (
        { type: EProductsAction.SET_PRODUCTS_LIST, productsList } as const
    ),
    /** Добвить доступные товары.
     * 
     * @param availableProducts
     */
    setAvailableProducts: (availableProducts: IProductModel[]) => (
        { type: EProductsAction.SET_AVAILABLE_PRODUCTS, availableProducts } as const
    ),
    /** Переключить индикатор загрузки товаров.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EProductsAction.TOGGLE_LOADING, value } as const
    )
};

/** Создать новый товар.
 * 
 * @param data
 */
export const createNewProduct = (data: any): ThunkType => (dispatch): any => {
    dispatch(productsAction.toggleLoading(true));
    API.POST('api/products', data)
        .then(() => {
            /** Обновить список точек */
            dispatch(requestAllProducts());
        })
        .catch(() => dispatch(productsAction.toggleLoading(false)));
};

/** Получить товар по ID.
 * 
 * @param id
 */
export const requestProductsById = (id: number): ThunkType => (dispatch): any => {
    dispatch(productsAction.toggleLoading(true));
    API.GET(`api/products/${id}`)
        .then((res: any) => {
            dispatch(productsAction.setProductCard(res.data));
            dispatch(productsAction.toggleLoading(false));
        })
        .catch(() => dispatch(productsAction.toggleLoading(false)));
};

/** Получить список товаров. */
export const requestAllProducts = (): ThunkType => (dispatch): any => {
    dispatch(productsAction.toggleLoading(true));
    API.GET('api/products')
        .then((res: any) => {
            dispatch(productsAction.addProductsList(res.data));
            dispatch(productsAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(productsAction.toggleLoading(false));
        });
};

/** Получить список доступных товаров.
 * 
 * @param marketId
 */
export const requestAvailableProducts = (marketId: number): ThunkType => (dispatch): any => {
    dispatch(productsAction.toggleLoading(true));
    API.GET(`api/products/available/${marketId}`)
        .then((res: any) => {
            dispatch(productsAction.setAvailableProducts(res.data));
            dispatch(productsAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(productsAction.toggleLoading(false));
        });
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof productsAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
