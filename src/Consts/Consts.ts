/** Домен. */
export const DOMAIN: string = 'http://localhost:5000/';

/** Енам страниц на админ панели. */
export enum EAdminViewContent {
    /** Аккаунты. */
    ACCOUNTS = 'ACCOUNTS',
    /** Пользователи. */
    USERS = 'USERS',
    /** Торговые точки. */
    MARKETS = 'MARKETS',
    /** Города. */
    CITIES = 'CITIES',
    /** Регионы. */
    REGIONS = 'REGIONS',
    /** Товары. */
    PRODUCTS = 'PRODUCTS',
}

/** Роли пользователей. */
export enum EUserRole {
    /** Продавец. */
    SELLER = 'SELLER',
    /** Управляющий по городу. */
    CITY_MANAGER = 'CITY_MANAGER',
    /** Управляющий по региону. */
    REGION_MANAGER = 'REGION_MANAGER',
    /** Главный управляющий. */
    GENERAL_MANAGER = 'GENERAL_MANAGER',
    /** Админ. */
    ADMIN = 'ADMIN'
}
