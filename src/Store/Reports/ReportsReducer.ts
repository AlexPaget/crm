import { API } from 'Api/Api';
import { BaseThunkType, store, TReducerAction } from 'Store/Store';

/** Actions - reportsReducer. */
export enum EReportAction {
    /** Переключить индиактор загрузки. */
    TOGGLE_LOADING = 'repotrs/TOGGLE_LOADING',
    /** Список отчетов. */
    SET_REPORTS_LIST = 'reports/SET_REPORTS_LIST',
    /** Список отчетов по дате. */
    SET_REPORTS_BY_DATE = 'reports/SET_REPORTS_BY_DATE',
    /** Список отчетов по городу. */
    SET_CITY_REPORTS = 'reports/SET_CITY_REPORTS',
    /** Список необработанных отчетов для региона. */
    SET_RAW_REPORTS_FOR_REGION = 'reports/SET_RAW_REPORTS_FOR_REGION',
    /** Добавить последние отчеты региона. */
    SET_LAST_REGION_REPORTS = 'reports/SET_LAST_REGION_REPORTS',
    /** Добавить все региональные отчеты. */
    SET_ALL_REGION_REPORTS = 'reports/SET_ALL_REGION_REPORTS',
    /** Добавить данные регионального отчета. */
    SET_REGION_REPORT_CARD = 'reports/SET_REGION_REPORT_CARD',
    /** Добавить данные регионального отчета. */
    CLEAR_REGION_REPORT_CARD = 'reports/CLEAR_REGION_REPORT_CARD',
    /** Добавить данные городского отчета. */
    SET_CITY_REPORT_CARD = 'reports/SET_CITY_REPORT_CARD',
    /** Добавить данные городского отчета. */
    CLEAR_CITY_REPORT_CARD = 'reports/CLEAR_CITY_REPORT_CARD'
};

/** Модель состояния. */
interface IState {
    /** Индикатор загрузки. */
    isLoading: boolean;
    /** Список отчетов. */
    reportsList: any[];
    /** Список отчетов. */
    reportsByDate: any[];
    /** Список отчетов города. */
    cityReports: any[];
    /** Список отчетов для региона. */
    rawReportsForRegion: any[];
    /** Последние отчеты региона. */
    lastRegionReports: any[];
    /** Все региональные отчеты. */
    allRegionReports: any[];
    /** Даные регионального отчета. */
    regionReportCard: any;
    /** Даные регионального отчета. */
    cityReportCard: any;
}

const initialState: IState = {
    isLoading: false,
    reportsList: [],
    reportsByDate: [],
    cityReports: [],
    rawReportsForRegion: [],
    lastRegionReports: [],
    allRegionReports: [],
    regionReportCard: null,
    cityReportCard: null,
};

/** Редюсер работы с отчетами.
 * 
 * @param state
 * @param action 
 */
export const reportsReducer = (
    state = initialState,
    action: TReducerAction<typeof reportAction>
): IState => {
    switch (action.type) {
        case (EReportAction.TOGGLE_LOADING):
            return {
                ...state,
                isLoading: action.value
            };
        case (EReportAction.SET_REPORTS_BY_DATE):
            return {
                ...state,
                reportsByDate: action.reportsByDate
            };
        case (EReportAction.SET_REPORTS_LIST):
            return {
                ...state,
                reportsList: action.reportsList
            };
        case (EReportAction.SET_CITY_REPORTS):
            return {
                ...state,
                cityReports: action.cityReports
            };
        case (EReportAction.SET_RAW_REPORTS_FOR_REGION):
            return {
                ...state,
                rawReportsForRegion: action.reports
            };
        case (EReportAction.SET_LAST_REGION_REPORTS):
            return {
                ...state,
                lastRegionReports: action.regionReports
            };
        case (EReportAction.SET_ALL_REGION_REPORTS):
            return {
                ...state,
                allRegionReports: action.regionReports
            };
        case (EReportAction.SET_REGION_REPORT_CARD):
            return {
                ...state,
                regionReportCard: action.report
            };
        case (EReportAction.CLEAR_REGION_REPORT_CARD):
            return {
                ...state,
                regionReportCard: null
            };
        case (EReportAction.SET_CITY_REPORT_CARD):
            return {
                ...state,
                cityReportCard: action.report
            };
        case (EReportAction.CLEAR_CITY_REPORT_CARD):
            return {
                ...state,
                cityReportCard: null
            };
        default:
            return state;
    }
};

export const reportAction = {
    /** Добавить отчеты по дате.
     * 
     * @param reportsByDate
     */
    setReportsByDate: (reportsByDate: any[]) => (
        { type: EReportAction.SET_REPORTS_BY_DATE, reportsByDate } as const
    ),
    /** Добавить список отчетов.
     * 
     * @param reportsList
     */
    setReportsList: (reportsList: any[]) => (
        { type: EReportAction.SET_REPORTS_LIST, reportsList } as const
    ),
    /** Добавить список отчетов.
     * 
     * @param reports
     */
    setRawReportsForRegion: (reports: any[]) => (
        { type: EReportAction.SET_RAW_REPORTS_FOR_REGION, reports } as const
    ),
    /** Добавить список последних отчетов региона.
     * 
     * @param regionReports
     */
    setLastRegionReports: (regionReports: any[]) => (
        { type: EReportAction.SET_LAST_REGION_REPORTS, regionReports } as const
    ),
    /** Добавить список последних отчетов региона.
     * 
     * @param regionReports
     */
    setAllRegionReports: (regionReports: any[]) => (
        { type: EReportAction.SET_ALL_REGION_REPORTS, regionReports } as const
    ),
    /** Добавить список последних отчетов региона.
     * 
     * @param report
     */
    setRegionReportCard: (report: any) => (
        { type: EReportAction.SET_REGION_REPORT_CARD, report } as const
    ),
    /** Очистить карточку городского отчета. */
    clearCityReportCard: () => ({ type: EReportAction.CLEAR_CITY_REPORT_CARD } as const),
    /** Добавить список последних отчетов города.
     * 
     * @param report
     */
    setCityReportCard: (report: any) => (
        { type: EReportAction.SET_CITY_REPORT_CARD, report } as const
    ),
    /** Очистить карточку регионального отчета. */
    clearRegionReportCard: () => ({ type: EReportAction.CLEAR_REGION_REPORT_CARD } as const),
    /** Добавить список отчетов города.
     * 
     * @param cityReports
     */
    setCityReports: (cityReports: any[]) => (
        { type: EReportAction.SET_CITY_REPORTS, cityReports } as const
    ),
    /** Переключить индикатор загрузки отчетов.
     * 
     * @param value Значение индикатора.
     */
    toggleLoading: (value: boolean) => (
        { type: EReportAction.TOGGLE_LOADING, value } as const
    ),
};

/** Создать новую смену.
 * 
 * @param marketId
 */
export const createNewReport = (marketId: number): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.POST('api/shifts', {
        marketId,
        sellerId: store.getState().users.currentUser?.id
    })
        .then(() => {
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Создать отчет города.
 * 
 * @param data
 */
export const createCityReport = (data: any): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.POST('api/reports/city', data)
        .then(() => {
            dispatch(requestReportsByDate(data.cityId, data.date));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};
/** Создать отчет региона.
 * 
 * @param data
 */
export const createRegionReport = (data: any): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.POST('api/reports/region', data)
        .then(() => {
            dispatch(requestRawReportsForRegion(data.regionId, data.date));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};
/** Получить отчеты за день.
 * 
 * @param cityId
 * @param date
 */
export const requestReportsByDate = (cityId: number, date: string): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/city/${cityId}/${date}`)
        .then((res: any) => {
            dispatch(reportAction.setReportsByDate(res.data));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Запросить список отчетов. */
export const requestReports = (): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET('api/reports')
        .then((res: any) => {
            dispatch(reportAction.toggleLoading(false));
            dispatch(reportAction.setReportsList(res.data));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Запросить список смен для отчета.
 * 
 * @param cityId
 * @param date
 */
export const requestRawShifts = (
    cityId: number,
    date?: string
): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/market-reports/${cityId}${date ? `/${date}` : ''}`)
        .then((res: any) => {
            dispatch(reportAction.toggleLoading(false));
            dispatch(reportAction.setReportsList(res.data));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Запросить список городских отчетов для региона.
 * 
 * @param regionId
 * @param date
 */
export const requestRawReportsForRegion = (
    regionId: number,
    date?: string
): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/city-reports/${regionId}${date ? `/${date}` : ''}`)
        .then((res: any) => {
            dispatch(reportAction.toggleLoading(false));
            dispatch(reportAction.setRawReportsForRegion(res.data));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Запросить список городских отчетов для региона.
 * 
 * @param regionId
 */
export const requestLastRegionReports = (regionId: number): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/region-reports/${regionId}`)
        .then((res: any) => {
            dispatch(reportAction.setLastRegionReports(res.data));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Запросить список региональных отчетов. */
export const requestAllRegionReports = (): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET('api/reports/region-reports')
        .then((res: any) => {
            dispatch(reportAction.setAllRegionReports(res.data));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};
/** Запросить данные регионального отчета.
 * 
 * @param id
 */
export const requestRegionReportById = (id: number): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/region-report/${id}`)
        .then((res: any) => {
            dispatch(reportAction.setRegionReportCard(res.data));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};
/** Запросить данные регионального отчета.
 * 
 * @param id
 */
export const requestCityReportById = (id: number): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/city-report/${id}`)
        .then((res: any) => {
            dispatch(reportAction.setCityReportCard(res.data));
            dispatch(reportAction.toggleLoading(false));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};
/** Запросить список отчетов города.
 * 
 * @param cityId
 */
export const requestCityReports = (cityId: number): ThunkType => (dispatch): any => {
    dispatch(reportAction.toggleLoading(true));
    API.GET(`api/reports/city-reports/${cityId}`)
        .then((res: any) => {
            dispatch(reportAction.toggleLoading(false));
            dispatch(reportAction.setCityReports(res.data));
        })
        .catch(() => {
            dispatch(reportAction.toggleLoading(false));
        });
};

/** Тип экшена. */
type ActionsTypes = TReducerAction<typeof reportAction>;
/** Тип для thunk. */
type ThunkType = BaseThunkType<ActionsTypes>;
