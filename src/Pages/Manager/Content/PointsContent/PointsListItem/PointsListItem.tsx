import { ICityModel } from 'Models/Models';
import './pointsListItem.scss';

/** Модель свойст компонента. */
interface IProps {
    /** Идентификатор. */
    id: number;
    /** Название. */
    title: string;
    /** Адресс */
    adress?: string;
    /** Колличество точек. */
    sellersCount?: number;
    /** Управляющий. */
    city?: ICityModel;
    /** Коллбек клика */
    onClick?: (id: number) => void;
}

/** Компонент элемента списка рабочих точек.
 * 
 * @param props
 */
export const PointsListItem = (props: IProps): JSX.Element => {
    const { id, title, adress, sellersCount, city, onClick } = props;

    /** Обработчик клика по элементу. */
    const handleItemLick = (): void => {
        onClick && onClick(id);
    };

    return (
        <div className="points-list-item" onClick={handleItemLick}>
            <span className="points-list-item__title">{title}</span>
            <span>{city?.name}</span>
            {adress && <span>{`Адрес: ${adress}`}</span>}
            <span>{`продавцов: ${sellersCount}`}</span>
        </div>
    );
};
